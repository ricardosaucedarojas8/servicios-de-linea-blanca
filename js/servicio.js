$(document).ready(function() {         
  $('#services').DataTable( { 
    "bDeferRender": true, 
    "order": [[ 0, "desc" ]],
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "../controller/serviceController.php",
      "type": "POST",
      "data": {"function":"getServices"},
    },          
    "columns": [
    { "data": "id" },
    { "data": "id_prospecto" },
    { "data": "acciones"},
    { "data": "contacto" },
    { "data": "asignado" },
    { "data": "precio" },
    { "data": "comision" },
    { "data": "total_refacciones" },
    { "data": "fecha" },
    { "data": "fecha_visita" }
    ],

    "oLanguage": {
      "sProcessing":     "Procesando...",
      "sLengthMenu": 'Mostrar <select>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">All</option>'+
      '</select> registros',    
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Filtrar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Por favor espere - cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }

    },

    dom: 'Bfrtip',
    buttons: [
    'copy', 'csv', 'excel', 'pdf', 'print'
    ]


  });


});

$(document).ready(function() {         
  $('#servicesLocation').DataTable( { 
    "bDeferRender": true, 
    "order": [[ 0, "desc" ]],
    "sPaginationType": "full_numbers",
    "ajax": {
      "url": "../controller/serviceController.php",
      "type": "POST",
      "data": {"function":"getServicesLocation"},
    },          
    "columns": [
    { "data": "id" },
    { "data": "id_prospecto" },
    { "data": "contacto" },
    { "data": "direccion" },
    { "data": "asignado" },
    { "data": "precio" },
    { "data": "total_refacciones" },
    { "data": "fecha" }
    ],

    "oLanguage": {
      "sProcessing":     "Procesando...",
      "sLengthMenu": 'Mostrar <select>'+
      '<option value="10">10</option>'+
      '<option value="20">20</option>'+
      '<option value="30">30</option>'+
      '<option value="40">40</option>'+
      '<option value="50">50</option>'+
      '<option value="-1">All</option>'+
      '</select> registros',    
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Filtrar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Por favor espere - cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }

    },

    dom: 'Bfrtip',
    buttons: [
    'copy', 'csv', 'excel', 'pdf', 'print'
    ]


  });


});



function createService(){
	var id_prospecto = $('#id_prospecto')[0].value;
	var id_empleado = $('#id_empleado')[0].value;
  var status = "1";

  if(id_empleado === "0"){
    $("#errorEmpleado").show();
    $("#id_empleado").focus();
  }else{
    var parametros = {
      "id_prospecto" : id_prospecto,
      "id_empleado" : id_empleado,
      "status" : status,
      "function" : "addService"
    };

    $.ajax({
        data:  parametros, //send data via AJAX
        url:   '../controller/serviceController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          //document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request

          if(response.success){ 
            $("#successModalTitle").html("<span class='text-success'><i class='fa fa-check-circle-o'></i> ¡Success!</span>");
            $("#successModalDescription").html(response.message);
          }else{
            $("#successModalTitle").html("<span class='text-danger'><i class='fa fa-times'></i> ¡Error!</span>");
            $("#successModalDescription").html(response.message);
          }
          
          $('#actionModal').modal('toggle');      
        }
      });

  	//alert(id_prospecto+"-"+id_empleado+"-"+status);
  }	
}

function serviceConfirm(){

  var id_servicio = $( "#id_servicio" ).val();
  var id_empleado = $( "#id_empleado" ).val();
  var id_prospecto = $( "#id_prospecto" ).val();
  var folio_garantia = $( "#folio_garantia" ).val();
  var folio = $( "#folio" ).val();
  var fecha_entrega = $( "#fecha_entrega" ).val();
  var id_tipo_servicio = $( "#id_tipo_servicio" ).val();
  var id_metodo_pago = $( "#id_metodo_pago" ).val();
  var diagnostico_final = $( "#diagnostico_final" ).val();
  var costo = $( "#costo" ).val();
  var correo = $( "#correo" ).val();
  var nombre = $( "#nombre" ).val();
  var equipo = $( "#equipo" ).val();
  var precio_fact = $( "#precio_fact" ).val();
  var anticipo = "";
  var marca = $( "#marca" ).val();
  var pago = $('input[name=pago]:checked').val();
  var factura = $('input[name=factura]:checked').val();
  var status = "";

  if(folio_garantia === ""){
    $("#errorFolio").show();
    $("#folio_garantia").focus();
  }else if(fecha_entrega === ""){
    $("#errorFolio").hide();
    $("#errorDate").show();
    $("#fecha_entrega").focus();
  }else if(id_tipo_servicio === "0"){
    $("#errorFolio").hide();
    $("#errorDate").hide();
    $("#errorService").show();
    $("#id_tipo_servicio").focus();
  }else if(diagnostico_final === ""){
    $("#errorService").hide();
    $("#errorDiagnostic").show();
    $("#diagnostico_final").focus();
  }else if($('input[name=factura]:checked').length == "0"){
    $("#errorDiagnostic").hide();
    $("#errorFactura").show();
    $("#factura").focus();
  }else if(id_metodo_pago === "0"){
    $("#errorFactura").hide();
    $("#errorMetod").show();
    $("#id_metodo_pago").focus();
  }else if(costo === "0"){
    $("#errorMetod").hide();
    $("#errorCost").show();
    $("#costo").focus();
  }else if($('input[name=pago]:checked').length == "0"){
    $("#errortotRefs").hide();
    $("#errorPago").show();
    $("#pago").focus();
  }else{

    if(factura === "si"){

      costo = precio_fact;
      factura = "1";
      //por si e necesita deshabilitar algun elemento
      //$('#anti').attr('disabled', 'disabled');

    }else if(factura==="no"){
      factura = "0";
    }

    if( pago === "completo"){
      anticipo = "0";
      status = "3";
    }else if(pago === "anticipo"){
      anticipo = $( "#adelanto" ).val();
      status = "2";
    }else if(pago === "revision"){
      anticipo = "0";
      status = "4";
      costo = 150;
    }

    var parametros = {
      "id_servicio" : id_servicio,
      "id_empleado" : id_empleado,
      "folio_garantia" : folio_garantia,
      "folio" : folio,
      "marca" : marca,
      "anticipo" : anticipo,
      "equipo" : equipo,
      "correo" : correo,
      "nombre" : nombre,
      "factura" : factura,
      "status" : status,
      "id_metodo_pago" : id_metodo_pago,
      "fecha_entrega" : fecha_entrega,
      "id_tipo_servicio" : id_tipo_servicio,
      "diagnostico_final" : diagnostico_final,
      "costo" : costo,
      "function" : "serviceConfirm"
    };

    $.ajax({
        data:  parametros, //send data via AJAX
        url:   '../../controller/serviceController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          //document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request

          if(response.success){ 
            $("#successModalTitle").html("<span class='text-success'><i class='fa fa-check-circle-o'></i> ¡Success!</span>");
            $("#successModalDescription").html(response.message);
          }else{
            $("#successModalTitle").html("<span class='text-danger'><i class='fa fa-times'></i> ¡Error!</span>");
            $("#successModalDescription").html(response.message);
          }
          
          $('#actionModal').modal('toggle');      
        }
      });
    



    /*alert("Servicio id: "+ id_servicio+ " FGarantia: " + folio_garantia + " Date " + fecha_entrega + " Tipo " + id_tipo_servicio +
      " Diag: " + diagnostico_final + " Costo " + costo + " Anticipo: " + anticipo + " Pago: " + pago + 
      " Id servicio " + id_servicio + " id_prospecto: " + id_prospecto);*/
    }

  }

  function confirm($id_usuario){
    $('.close').click(); 
    window.location="index.php?suc=true&idu="+$id_usuario;
  }

  function confirmService($id_empleado){
    $('.close').click(); 

  //se coloca *?ser=* para sustituir el nombre de ID_EMPLEADO
  window.location="services.php?ser="+$id_empleado;
}

$(document).ready(function(){
  $("#subtotal").keyup(function(){
    var costo = $('#subtotal')[0].value;

    if (parseFloat(costo) > 0) {
      document.getElementById('showTotal').style.visibility = 'visible';
    }else{
      document.getElementById('showTotal').style.visibility = 'hidden';
    }

    var operacion = parseFloat(costo) * 0.30;
    var impuesto = parseFloat(costo) * 0.16;
    var fact =   parseFloat(costo)+impuesto;
    var comision = parseFloat(costo) * 0.05;
    var total = parseFloat(comision) + parseFloat(costo);

    $("#adelanto").val(operacion);
    //$(".display").text('$' + operacion);
    $(".display_fact").text('$' + fact);
    $("#precio_fact").val(fact);
    $("#comision").val(comision);
    $("#costo").val(costo);
    $(".display_total").text('$' + total);
  });
});





function showTotal(){
  $('#exampleModal').modal('toggle');
}

function terminarServicio(data){

  $('#confirmPendiente').val('');
  $('#refacciones').val('');

  var id_servicio = $(data).attr("data-id-servicio");
  var anticipo = $(data).attr("data-anticipo");
  var subtotal = $(data).attr("data-precio");
  var correo = $(data).attr("data-correo");
  var comision = $(data).attr("data-comision");
  var marca = $(data).attr("data-marca");
  var equipo = $(data).attr("data-equipo");
  var id_tipo_servicio = $(data).attr("data-id-tipo-servicio");
  var nombre_completo = $(data).attr("data-nombre-completo");
  var fecha_entrega = $(data).attr("data-fecha-entrega");
  var folio = $(data).attr("data-folio");
  var folio_garantia = $(data).attr("data-folio-garantia");

  var precio = parseFloat(subtotal) + parseFloat(comision);
  var pendiente = parseFloat(precio) - parseFloat(anticipo);


  $("#id_servic").val(id_servicio);
  $("#pendiente").val(pendiente);
  $("#email").val(correo);
  $("#nombre_marca").val(marca);
  $("#tipo_equipo").val(equipo);
  $("#fecha_entrega").val(fecha_entrega);
  $("#num_folio").val(folio);
  $("#folio_garantia").val(folio_garantia);
  $("#id_tipo_servicio").val(id_tipo_servicio);
  $("#nombre_completo").val(nombre_completo);
  $("#endModalTitle").html(" Terminar Servicio");
  $("#endModalDescription").html('Folio del Servicio: (<b>' + folio + '</b>)<br><br>La cuenta total es: <b>$' + precio + "</b><br>Anticipo 1er Visita: <b>$" + anticipo + "</b><br>");
  $("#labelPending").html("<b>PENDIENTE POR LIQUIDAR: $"+ pendiente + "</b><br>");
  $('#endProcess').modal('toggle');   


}

function confirmEndService(){

  var id_servicio = $( "#id_servic" ).val();
  var correo = $( "#email" ).val();
  var nombre_completo = $( "#nombre_completo" ).val();
  var equipo = $( "#tipo_equipo" ).val();
  var marca = $( "#nombre_marca" ).val();
  var folio = $( "#num_folio" ).val();
  var folio_garantia = $( "#folio_garantia" ).val();
  var fecha_entrega = $( "#fecha_entrega" ).val();
  var id_tipo_servicio = $( "#id_tipo_servicio" ).val();
  var pendiente = $( "#pendiente" ).val();
  var confirmPendiente = $( "#confirmPendiente" ).val();
  var refacciones = $( "#refacciones" ).val();

   // Get the forms we want to add validation styles to
   var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {

      if (form.checkValidity() === false) {
          //$('#actionModal').modal('toggle');
          event.preventDefault();
          event.stopPropagation();

        }else{

          if (pendiente == confirmPendiente) {

            $('.close').click(); 

            var parametros = {
              "id_servicio" : id_servicio,
              "correo" : correo,
              "nombre_completo" : nombre_completo,
              "equipo" : equipo,
              "marca" : marca,
              "folio" : folio,
              "folio_garantia" : folio_garantia,
              "fecha_entrega" : fecha_entrega,
              "id_tipo_servicio" : id_tipo_servicio,
              "refacciones" : refacciones,
              "function" : "endService"
            };

            $.ajax({
              data:  parametros, //send data via AJAX
              url:   '../../controller/serviceController.php', //url file controller PHP
              type:  'post', //send POST data
              beforeSend: function () {
                //document.getElementById("load").style.display = "block";
              },
              success:  function (response) { //get request

                if(response.success){ 
                  $("#successModalTitle").html("<span class='text-success'><i class='fa fa-check-circle-o'></i> ¡Success!</span>");
                  $("#successModalDescription").html(response.message);
                }else{
                  $("#successModalTitle").html("<span class='text-danger'><i class='fa fa-times'></i> ¡Error!</span>");
                  $("#successModalDescription").html(response.message);
                }
                
                $('#actionModal').modal('toggle');

              }
            });

          }else{  
            //PODRIA BORRAR EL VALUE INGRESADO PA QUE LA VALIDACION SE ACTIVE DE INMEDIATO SOLA---- PENDIENTE POR CHECAR
            //document.getElementById('confirmPendiente').reset();
            $("#confirmPendiente").val("");

          }

        }
        form.classList.add('was-validated');
      });

  }


  function confirmEnd($id_usuario){
    $('.close').click(); 
    $("#servic").load("../../views/technics/services.php?ser="+$id_usuario+ "  #servic");
  }

  function edithTechnic(data){

    $('#edithTechnic').modal('toggle');

    var id_empleado = $(data).attr("data-id-empleado");
    var id_servicio = $(data).attr("data-id");
    var nombre_completo = $(data).attr("data-nombre-empleado");  
    $("#technicText").html("El técnico actual es: <b>" + nombre_completo +"</b>");
    $("#technicDescription").html("<b>Asigna un Técnico</b>");
    $("#id_servic").val(id_servicio);

    $("select#id_emp")[0].selectedIndex = 0; 


  }


  function selectTechnic(data){

    $('#edithTechnic').modal('hide');

    var id_empleado = $( "#id_emp" ).val();
    var id_servicio = $( "#id_servic" ).val();

    var parametros = {
      "id_empleado" : id_empleado,
      "id_servicio" : id_servicio,
      "function" : "edithTechnic"
    };
  //alert(id_empleado + " - " + id_servicio);


  $.ajax({
        data:  parametros, //send data via AJAX
        url:   '../controller/serviceController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          //document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request

          if(response.success){ 
            $("#successModalTitle").html("<span class='text-success'><i class='fa fa-check-circle-o'></i> ¡Success!</span>");
            $("#successModalDescription").html(response.message);

            $('#services').DataTable().ajax.reload();
          }else{
            $("#successModalTitle").html("<span class='text-danger'><i class='fa fa-times'></i> ¡Error!</span>");
            $("#successModalDescription").html(response.message);
          }
          
          $('#actionModal').modal('toggle');  

        }
      });
}


function showInfoService(data){

  var direccion = $(data).attr("data-direc");
  var correo = $(data).attr("data-correo");
  var equipo = $(data).attr("data-equipo");
  var marca = $(data).attr("data-marca");
  var cp = $(data).attr("data-cp");
  var tecnico = $(data).attr("data-nombre-empleado");
  var antecedente = $(data).attr("data-antecedente");

  $('#modalInfoService').html('<b>Antecedente: </b>' + antecedente + '<br><b>CP: </b>' + cp + '<b> Direccion: </b> ' + direccion + '<br><b>Correo: </b>' + correo );

  $('#showInfoService').modal('toggle');
}


function addGuarantee(data){
  $('#activarGarantia').modal('toggle');  
}

function updateMarc(data){

  $('#updateMarc').modal('toggle');

  var id_marca = $(data).attr("data-id-marca");
  var marca = $(data).attr("data-marca");
  var id_prospecto = $(data).attr("data-id-prospecto");
  $("#marcText").html("La marca actual del equipo es: <b>" + marca +"</b>");
  $("#marcDescription").html("Actualiza la <b>marca</b> del servicio <b>p" + id_prospecto +"</b>");
  $("#id_prospecto").val(id_prospecto);

}

function selectMarc(data){

  $('#updateMarc').modal('hide');

  var id_prospecto = $( "#id_prospecto" ).val();
  var id_marca = $( "#id_mar" ).val();

  var parametros = {
    "id_prospecto" : id_prospecto,
    "id_marca" : id_marca,
    "function" : "edithMarc"
  };

  $.ajax({
        data:  parametros, //send data via AJAX
        url:   '../controller/serviceController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          //document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request

          if(response.success){ 
            $("#successModalTitle").html("<span class='text-success'><i class='fa fa-check-circle-o'></i> ¡Success!</span>");
            $("#successModalDescription").html(response.message);

            $('#services').DataTable().ajax.reload();
          }else{
            $("#successModalTitle").html("<span class='text-danger'><i class='fa fa-times'></i> ¡Error!</span>");
            $("#successModalDescription").html(response.message);
          }
          
          $('#actionModal').modal('toggle');  

        }
      });
}


// Disable form submissions if there are invalid fields
function serviceC(){ 
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {

      if (form.checkValidity() === false) {
          //$('#actionModal').modal('toggle');
          event.preventDefault();
          event.stopPropagation();

        }else{

          var id_servicio = $( "#id_servicio" ).val();
          var id_empleado = $( "#id_empleado" ).val();
          var id_prospecto = $( "#id_prospecto" ).val();
          var folio_garantia = $( "#folio_garantia" ).val();
          var folio = $( "#folio" ).val();
          var fecha_entrega = $( "#fecha_entrega" ).val();
          var id_tipo_servicio = $( "#id_tipo_servicio" ).val();
          var id_metodo_pago = $( "#id_metodo_pago" ).val();
          var diagnostico_final = $( "#diagnostico_final" ).val();
          var costo = $( "#costo" ).val();
          var correo = $( "#correo" ).val();
          var nombre = $( "#nombre" ).val();
          var equipo = $( "#equipo" ).val();
          var precio_fact = $( "#precio_fact" ).val();
          var anticipo = "";
          var marca = $( "#marca" ).val();
          var pago = $('input[name=pago]:checked').val();
          var factura = $('input[name=factura]:checked').val();
          var totalRefacciones = $( "#totRefaccion" ).val();
          var comision = $( "#comision" ).val();
          var status = "";


          if(factura === "si"){

            costo = precio_fact;
            factura = "1";
            comision = 0;
            //por si e necesita deshabilitar algun elemento
            //$('#anti').attr('disabled', 'disabled');

          }else if(factura==="no"){
            factura = "0";
          }

          if( pago === "completo"){
            anticipo = "0";
            status = "3";
          }else if(pago === "anticipo"){
            anticipo = $( "#adelanto" ).val();
            status = "2";
          }else if(pago === "revision"){
            anticipo = "0";
            status = "4";
            costo = 150;
          }

          var parametros = {
            "id_servicio" : id_servicio,
            "id_empleado" : id_empleado,
            "folio_garantia" : folio_garantia,
            "folio" : folio,
            "marca" : marca,
            "anticipo" : anticipo,
            "equipo" : equipo,
            "correo" : correo,
            "comision" : comision,
            "nombre" : nombre,
            "factura" : factura,
            "status" : status,
            "total_refacciones" : totalRefacciones,
            "id_metodo_pago" : id_metodo_pago,
            "fecha_entrega" : fecha_entrega,
            "id_tipo_servicio" : id_tipo_servicio,
            "diagnostico_final" : diagnostico_final,
            "costo" : costo,
            "function" : "serviceConfirm"
          };

          $.ajax({
        data:  parametros, //send data via AJAX
        url:   '../../controller/serviceController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          //document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request

          if(response.success){ 
            $("#successModalTitle").html("<span class='text-success'><i class='fa fa-check-circle-o'></i> ¡Success!</span>");
            $("#successModalDescription").html(response.message);
          }else{
            $("#successModalTitle").html("<span class='text-danger'><i class='fa fa-times'></i> ¡Error!</span>");
            $("#successModalDescription").html(response.message);
          }
          
          $('#actionModal').modal('toggle');      
        }
      });
          
    /*alert("Servicio id: "+ id_servicio+ " FGarantia: " + folio_garantia + " Date " + fecha_entrega + " Tipo " + id_tipo_servicio +
      " Diag: " + diagnostico_final + " Costo " + costo + " Anticipo: " + anticipo + " Pago: " + pago + 
      " Id servicio " + id_servicio + " id_prospecto: " + id_prospecto);*/


    }
    form.classList.add('was-validated');
  });

  }


  function hideTFR(){
    document.getElementById('divRefaccion').style.display = 'none';
  }

  function showTFR(){
    document.getElementById('divRefaccion').style.display = 'block';
  }