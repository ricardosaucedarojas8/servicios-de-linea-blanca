function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}

function doAction(){
	var nombre = $('#nombre')[0].value;
	var empresa = $('#empresa')[0].value;
  var telefono = $('#telefono')[0].value;
  var equipo = $('#id_equipo')[0].value;
  var correo = $('#correo')[0].value;
  var direccion = $('#direccion')[0].value;
  var cp = $('#cp')[0].value;
  var id_marca = $('#id_marca')[0].value;
  var id_usuario = $('#id_usuario')[0].value;
  var hr_visita = $('#hr_visita')[0].value;
  var diagnostico = $('#diagnostico')[0].value;
  var status = "1";
  var folio = "f1";
  var fecha_visita = $('#fecha_visita')[0].value;

  
  var tam_tel = $('#telefono').val().length;
  var tam_cp = $('#cp').val().length;

  if(nombre === ""){
    $("#errorName").show();
    $("#nombre").focus();
  }else if(empresa === ""){
    $("#errorName").hide();
    $("#errorEnterprise").show();
    $("#empresa").focus();
  }else if(telefono === "" || tam_tel < 9 || tam_tel > 11 || !$.isNumeric(telefono) ){
    $("#errorEnterprise").hide();
    $("#errorPhone").show();
    $("#telefono").focus();
  }else if( correo === "" || !validateEmail(correo) ) {
    $("#errorPhone").hide();
    $("#errorEmail").show();
    $("#correo").focus();
  }else if(direccion === ""){
    $("#errorEmail").hide();
    $("#errorDirection").show();
    $("#direccion").focus();
  }else if(cp === "" || tam_cp < 5 || !$.isNumeric(cp) ){
    $("#errorDirection").hide();
    $("#errorCp").show();
    $("#cp").focus();
  }else if(fecha_visita === ""){
    $("#errorCp").hide();
    $("#errorDate").show();
    $("#fecha_visita").focus();
  }else if(hr_visita === ""){
    $("#errorDate").hide();
    $("#errorHour").show();
    $("#hr_visita").focus();
  }else if(id_marca === "0"){
    $("#errorHour").hide();
    $("#errorBrand").show();
    $("#id_marca").focus();
  }else if(equipo === "0"){
    $("#errorBrand").hide();
    $("#errorDevice").show();
    $("#id_equipo").focus();
  }else if(diagnostico === ""){
    $("#errorDevice").hide();
    $("#errorDiagnostic").show();
    $("#diagnostico").focus();
  }else{
  //alert(nombre+"-"+empresa+"-"+telefono+"-"+equipo+"-"+correo+"-"+cp+"-"+id_marca+"-"+id_usuario+"-"+direccion+"-"+hr_visita+"-"+diagnostico+"-"+status+"-"+folio+"-"+fecha_visita);

  var parametros = {
    "id_marca" : id_marca,
    "id_usuario" : id_usuario,
    "nombre_completo" : nombre,
    "direccion" : direccion,
    "telefono" : telefono,
    "correo" : correo,
    "empresa" : empresa,
    "id_equipo" : equipo,
    "cp" : cp,
    "folio" : folio,
    "fecha_de_visita" : fecha_visita,
    "hr_de_visita" : hr_visita,
    "status" : status,
    "diagnostico" : diagnostico,
    "function" : "addProspect"
  };
  $.ajax({
        data:  parametros, //send data via AJAX
        url:   '../controller/prospectController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          //document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request

          if(response.success){ 
           $('#actionModal').modal('toggle');
           $('#form_prospect').trigger("reset"); 
           $("#propectos_refresh").load("../views/index.php?suc=true&idu="+id_usuario+" #propectos_refresh");
           $("#num_prospectos").load("../views/index.php?suc=true&idu="+id_usuario+" #num_prospectos");
           $("#successModalTitle").html("<i class='fas fa-check color-success'></i> Éxito!");
           $("#successModalDescription").html(response.message);

         }else{
          $("#successModalTitle").html("<i class='fas fa-exclamation-circle color-error'></i> ¡Error!");
          $("#successModalDescription").html(response.message);
          //alert(response.message);
          $('#actionModal').modal('toggle'); 
        }       
      }
    });

}

}


// Disable form submissions if there are invalid fields
function sendForm(){ 
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {

      if (form.checkValidity() === false) {
          //$('#actionModal').modal('toggle');
          event.preventDefault();
          event.stopPropagation();

        }else{

          var nombre = $('#nombre').val();
          var empresa = $('#empresa').val();
          var telefono = $('#telefono').val();
          var equipo = $('#id_equipo').val();
          var correo = $('#correo').val();
          var direccion = $('#direccion').val();
          var cp = $('#cp').val();
          var id_marca = $('#id_marca').val();
          var id_sucursal = $('#id_sucursal').val();
          var id_usuario = $('#id_usuario').val();
          var hr_visita = $('#hr_visita').val();
          var diagnostico = $('#diagnostico').val();
          var status = "1";
          var fecha_visita = $('#fecha_visita').val();


          //AQUI ME FALTA METER EL AJAX pa mandar los datos al Controller

          //alert('-> ' + id_sucursal + ' ' );


          var parametros = {
            "id_marca" : id_marca,
            "id_usuario" : id_usuario,
            "id_sucursal" : id_sucursal,
            "nombre_completo" : nombre,
            "direccion" : direccion,
            "telefono" : telefono,
            "correo" : correo,
            "empresa" : empresa,
            "id_equipo" : equipo,
            "cp" : cp,
            "fecha_de_visita" : fecha_visita,
            "hr_de_visita" : hr_visita,
            "status" : status,
            "diagnostico" : diagnostico,
            "function" : "addProspect"
          };
          $.ajax({
              data:  parametros, //send data via AJAX
              url:   '../controller/prospectController.php', //url file controller PHP
              type:  'post', //send POST data
              beforeSend: function () {
                //document.getElementById("load").style.display = "block";
              },
              success:  function (response) { //get request

                if(response.success){ 
                 $('#actionModal').modal('toggle');
                 $('#form_prospect').trigger("reset"); 
                 $("#propectos_refresh").load("../views/index.php?suc=true&idu="+id_usuario+" #propectos_refresh");
                 $("#num_prospectos").load("../views/index.php?suc=true&idu="+id_usuario+" #num_prospectos");
                 $("#num_prospectos").addClass("text-success");
                 $("#successModalTitle").html("<span class='text-success'><i class='fas fa-exclamation-triangle'></i> Éxito! </span>");
                 $("#successModalDescription").html(response.message);
                 form.classList.remove('was-validated');

               }else{
                $("#successModalTitle").html("<i class='fas fa-exclamation-circle color-error'></i> ¡Error!");
                $("#successModalDescription").html(response.message);
                //alert(response.message);
                $('#actionModal').modal('toggle'); 
              }       
            }
          });

        }
        form.classList.add('was-validated');
      });

  }


  function confirm(){
    $('.close').click();
  }

  function asignarServicio(data){
    var id_prospecto = $(data).attr("data-id");
    var id_usuario = $(data).attr("data-id-usuario");

  //alert(id_prospecto);
  window.location.replace("servicios.php?pros="+id_prospecto+"&idu="+id_usuario);


}

function showProspect(data){
  $('#edithProspect').modal('toggle');   

  var id_prospecto = $(data).attr("data-id");
  var nombre = $(data).attr("data-nombre-completo");
  var empresa = $(data).attr("data-empresa");
  var telefono = $(data).attr("data-tel");
  var equipo = $(data).attr("data-id-equipo");
  var correo = $(data).attr("data-correo");
  var direccion = $(data).attr("data-direc");
  var cp = $(data).attr("data-cp");
  var id_marca = $(data).attr("data-id-marca");
  
  var hr_visita = $(data).attr("data-hora");
  var diagnostico = $(data).attr("data-diagnostico");
  var fecha_visita = $(data).attr("data-fecha-visita");

  $("#nombre_completo_edt").val(nombre);
  $("#empresa_edt").val(empresa);
  $("#telefono_edt").val(telefono);
  $("#correo_edt").val(correo);
  $("#cp_edt").val(cp);
  $("select#id_marca_edt").val(id_marca);
  $("select#id_equipo_edt").val(equipo); 
  $("#direccion_edt").val(direccion);
  $("#hora_visita_edt").val(hr_visita);
  $("#fecha_visita_edt").val(fecha_visita);
  $("#diagnostico_edt").val(diagnostico);
  $("#id_prospecto_edt").val(id_prospecto);


}

function showProspectService(data){
  $('#edithProspect').modal('toggle');   

  var id_prospecto = $(data).attr("data-p-id-prospecto");
  var nombre = $(data).attr("data-p-nombre");
  var empresa = $(data).attr("data-p-empresa");
  var telefono = $(data).attr("data-p-telefono");
  var equipo = $(data).attr("data-p-id-equipo");
  var correo = $(data).attr("data-p-correo");
  var direccion = $(data).attr("data-p-direc");
  var cp = $(data).attr("data-p-cp");
  var id_marca = $(data).attr("data-p-id-marca");
  
  var hr_visita = $(data).attr("data-p-hr-visita");
  var diagnostico = $(data).attr("data-p-diagnostico");
  var fecha_visita = $(data).attr("data-p-fecha-visita");

  $("#nombre_completo_edt").val(nombre);
  $("#empresa_edt").val(empresa);
  $("#telefono_edt").val(telefono);
  $("#correo_edt").val(correo);
  $("#cp_edt").val(cp);
  $("select#id_marca_edt").val(id_marca);
  $("select#id_equipo_edt").val(equipo); 
  $("#direccion_edt").val(direccion);
  $("#hora_visita_edt").val(hr_visita);
  $("#fecha_visita_edt").val(fecha_visita);
  $("#diagnostico_edt").val(diagnostico);
  $("#id_prospecto_edt").val(id_prospecto);


}

function edithProspect(){

  var id_prospecto = $('#id_prospecto_edt').val();
  var nombre = $('#nombre_completo_edt')[0].value;
  var empresa = $('#empresa_edt')[0].value;
  var telefono = $('#telefono_edt')[0].value;
  var equipo = $('#id_equipo_edt')[0].value;
  var correo = $('#correo_edt')[0].value;
  var direccion = $('#direccion_edt')[0].value;
  var cp = $('#cp_edt')[0].value;
  var id_marca = $('#id_marca_edt')[0].value;
  var id_usuario = $('#id_usuario_edt')[0].value;
  var hr_visita = $('#hora_visita_edt')[0].value;
  var diagnostico = $('#diagnostico_edt')[0].value;
  var fecha_visita = $('#fecha_visita_edt')[0].value;

  var tam_tel = $('#telefono_edt').val().length;
  var tam_cp = $('#cp_edt').val().length;

  if(nombre === ""){
    $("#edithErrorName").show();
    $("#nombre_completo_edt").focus();
  }else if(empresa === ""){
    $("#edithErrorName").hide();
    $("#edithErrorEnterprise").show();
    $("#empresa_edt").focus();
  }else if(telefono === "" || tam_tel < 9 || tam_tel > 11 || !$.isNumeric(telefono) ){
    $("#edithErrorEnterprise").hide();
    $("#edithErrorPhone").show();
    $("#telefono_edt").focus();
  }else if( correo === "" || !validateEmail(correo) ) {
    $("#edithErrorPhone").hide();
    $("#edithErrorEmail").show();
    $("#correo_edt").focus();
  }else if(direccion === ""){
    $("#edithErrorEmail").hide();
    $("#edithErrorDirection").show();
    $("#direccion_edt").focus();
  }else if(cp === "" || tam_cp < 5 || !$.isNumeric(cp) ){
    $("#edithErrorDirection").hide();
    $("#edithErrorCp").show();
    $("#cp_edt").focus();
  }else if(fecha_visita === ""){
    $("#edithErrorCp").hide();
    $("#edithErrorDate").show();
    $("#fecha_visita_edt").focus();
  }else if(hr_visita === ""){
    $("#edithErrorDate").hide();
    $("#edithErrorHour").show();
    $("#hora_visita_edt").focus();
  }else if(id_marca === "0"){
    $("#edithErrorHour").hide();
    $("#edithErrorBrand").show();
    $("#id_marca_edt").focus();
  }else if(equipo === "0"){
    $("#edithErrorBrand").hide();
    $("#edithErrorDevice").show();
    $("#id_equipo_edt").focus();
  }else if(diagnostico === ""){
    $("#edithErrorDevice").hide();
    $("#edithErrorDiagnostic").show();
    $("#diagnostico_edt").focus();
  }else{

    $("#edithErrorDiagnostic").hide();
    
    var parametros = {
      "id_marca" : id_marca,
      "id_prospecto" : id_prospecto,
      "nombre_completo" : nombre,
      "direccion" : direccion,
      "telefono" : telefono,
      "correo" : correo,
      "empresa" : empresa,
      "id_equipo" : equipo,
      "cp" : cp,
      "fecha_de_visita" : fecha_visita,
      "hr_de_visita" : hr_visita,
      "diagnostico" : diagnostico,
      "function" : "edithProspect"
    };
    $.ajax({
        data:  parametros, //send data via AJAX
        url:   '../controller/prospectController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          //document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request

          if(response.success){ 
            $('#edithProspect').modal('hide');
            $('#actionModal').modal('toggle');
            $("#propectos_refresh").load("../views/index.php?suc=true&idu="+id_usuario+" #propectos_refresh");
            $("#num_prospectos").load("../views/index.php?suc=true&idu="+id_usuario+" #num_prospectos");
            $("#successModalTitle").html("<i class='fas fa-check color-success'></i> Éxito!");
            $("#successModalDescription").html("Hemos editado el Prospecto con Éxito...");

          }else{

            $('#edithProspect').modal('hide');

            $("#successModalTitle").html("<i class='fas fa-exclamation-circle color-error'></i> ¡Error!");
            $("#successModalDescription").html(response.message);
          //alert(response.message);
          
          $('#actionModal').modal('toggle'); 
        }       
      }
    });

  }


}


