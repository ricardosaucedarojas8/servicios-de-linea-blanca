$(document).ready(function() {			   
	$('#checkip').DataTable( {	
		"bDeferRender": true,	
		"order": [[ 0, "desc" ]],
		"sPaginationType": "full_numbers",
		"ajax": {
			"url": "../controller/checkipController.php",
        	"type": "POST",
        	"data": {"function":"getCheckIP"},
		},					
		"columns": [
			{ "data": "id_check" },
			{ "data": "ip_number" },
			{ "data": "ip_counter" },
			{ "data": "ip_date"},
			{ "data": "ip_page_visit"},
			{ "data": "ip_status" },
			{ "data": "acciones" }
		],

		"oLanguage": {
            "sProcessing":     "Procesando...",
		    "sLengthMenu": 'Mostrar <select>'+
		        '<option value="10">1const copyText = document.querySelector("#copyMe");0</option>'+
		        '<option value="20">20</option>'+
		        '<option value="30">30</option>'+
		        '<option value="40">40</option>'+
		        '<option value="50">50</option>'+
		        '<option value="-1">All</option>'+
		        '</select> registros',    
		    "sZeroRecords":    "No se encontraron resultados",
		    "sEmptyTable":     "Ningún dato disponible en esta tabla",
		    "sInfo":           "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
		    "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		    "sInfoPostFix":    "",
		    "sSearch":         "Filtrar:",
		    "sUrl":            "",
		    "sInfoThousands":  ",",
		    "sLoadingRecords": "Por favor espere - cargando...",
		    "oPaginate": {
		        "sFirst":    "Primero",
		        "sLast":     "Último",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    },
		    "oAria": {
		        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		    }
		    
        },

        dom: 'Bfrtip',
        buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
        ]
	});
});


function checkIP(element){
  var $temp = $("<input>");
 $("body").append($temp);
 $temp.val($(element).html()).select();
 document.execCommand("copy");
 $('.success').toast('show'); 
 $temp.remove();
  

}