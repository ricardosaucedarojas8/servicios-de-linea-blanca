var input = document.getElementById("pass-login");

input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById("btnTechLogin").click();
  }
});


function doAction(){
	var correo = $('#email-login')[0].value;
	var password = $('#pass-login')[0].value;
	

	var parametros = {
        "email-login" : correo,
        "pass-login" : password,
        "function" : "getAccess"
      };
      $.ajax({
        data:  parametros, //send data via AJAX
        url:   '../../controller/loginTechController.php', //url file controller PHP
        type:  'post', //send POST data
        beforeSend: function () {
          document.getElementById("load").style.display = "block";
        },
        success:  function (response) { //get request
          
        if(response.success){ 
        	var id_empleado = response.id_empleado;
        	window.location.href = "../../views/technics/services.php?ser="+id_empleado; 
	       
        }else{
          $("#successModalTitle").html("<i class='fas fa-exclamation-circle color-error'></i> ¡Error!");
          $("#successModalDescription").html(response.message);
          //alert(response.message);
          $('#alertModal').modal('toggle'); 
        } 
              
        }
      });

}

function confirm(){
  $('.close').click();
  document.getElementById("load").style.display = "none";
}