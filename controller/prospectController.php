<?php



if(isset($_POST['function']) && !empty($_POST['function'])) {

	$function = $_POST['function'];
    
    
	//En función del parámetro que nos llegue ejecutamos una función u otra
    switch($function) {

        case 'addProspect':
        	# code...
        	$id_marca = $_POST['id_marca'];
        	$id_usuario = $_POST['id_usuario'];
            $id_sucursal = $_POST['id_sucursal'];
        	$nombre_completo = $_POST['nombre_completo'];
        	$direccion = $_POST['direccion'];
        	$telefono = $_POST['telefono'];
        	$correo = $_POST['correo'];
            $cp = $_POST['cp'];
        	$empresa = $_POST['empresa'];
        	$equipo = $_POST['id_equipo'];
        	$fecha_de_visita = $_POST['fecha_de_visita'];
        	$diagnostico = $_POST['diagnostico'];
        	$fecha_registro = date("Y-m-d");
        	$hora_de_visita = $_POST['hr_de_visita'];
        	$status = 1;

        	require_once 'conn/connection.php';
        	$connect = new connection();
			$connection=$connect->connections();
			
			
			$sql = "INSERT INTO prospectos (id_marca, id_usuario, id_sucursal, nombre_completo, direccion, telefono, correo, empresa, id_equipo, fecha_de_visita, fecha_registro, hora_de_visita, status, diagnostico, cp) VALUES ('".$id_marca ."','".$id_usuario."','".$id_sucursal."','".$nombre_completo."','".$direccion."','".$telefono."','".$correo ."', '".$empresa."', '".$equipo."','".$fecha_de_visita."','".$fecha_registro."','".$hora_de_visita."','".$status."','".$diagnostico."','".$cp."');";
			

			$jsondata = array();


        	if ($connection->query($sql)===true) {
        		$message= "El Prospecto ha sido guardado correctamente.";
			    $jsondata['success'] = true;
        		$jsondata['message'] = $message;

			} else {
				$message= "El Prospecto ha tenido un problema al ser guardado, contacta al servicio técnico.";
			    $jsondata['success'] = false;
        		$jsondata['message'] = $message;
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);

        	break;

        case 'edithProspect':

            # code...
            $id_marca = $_POST['id_marca'];
            $id_prospecto = $_POST['id_prospecto'];
            $nombre_completo = $_POST['nombre_completo'];
            $direccion = $_POST['direccion'];
            $telefono = $_POST['telefono'];
            $correo = $_POST['correo'];
            $cp = $_POST['cp'];
            $empresa = $_POST['empresa'];
            $id_equipo = $_POST['id_equipo'];
            $fecha_de_visita = $_POST['fecha_de_visita'];
            $diagnostico = $_POST['diagnostico'];
            $hora_de_visita = $_POST['hr_de_visita'];

            require_once 'conn/connection.php';
            $connect = new connection();
            $connection=$connect->connections();
            
            
            $sql = "UPDATE prospectos SET id_marca='$id_marca', nombre_completo='$nombre_completo', fecha_de_visita='$fecha_de_visita', diagnostico='$diagnostico', direccion='$direccion', telefono='$telefono', hora_de_visita='$hora_de_visita', cp='$cp', correo='$correo', empresa='$empresa', id_equipo='$id_equipo' WHERE id_prospecto='$id_prospecto'";
            

            $jsondata = array();


            if ($connection->query($sql)===true) {
                $message= "El Prospecto ha sido editado correctamente.";
                $jsondata['success'] = true;
                $jsondata['message'] = $message;

            } else {
                $message= "El Prospecto ha tenido un problema al ser editado, contacta al servicio técnico.";
                $jsondata['success'] = false;
                $jsondata['message'] = $message;
            }
            //Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
            header('Content-type: application/json; charset=utf-8');
            
            echo json_encode($jsondata, JSON_FORCE_OBJECT);

            break;
      
    }


}
		
?>