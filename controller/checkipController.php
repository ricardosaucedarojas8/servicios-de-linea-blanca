<?php



if(isset($_POST['function']) && !empty($_POST['function'])) {

	$function = $_POST['function'];
    
    
	//En función del parámetro que nos llegue ejecutamos una función u otra
    switch($function) {

        case 'getCheckIP': 
        	
        	require_once 'conn/connection.php';
	        
            $connect = new connection();
			$connection=$connect->connections();

			 $sql = "SELECT id_check, ip_number, ip_counter, ip_date, ip_status, ip_page_visit FROM checkip ORDER BY id_check DESC";

			$result = mysqli_query($connection, $sql);

			$tabla = "";
			
			while($row = mysqli_fetch_array($result)){
				$id_check = $row['id_check'];
				$ip_number = $row['ip_number'];
				$ip_counter = $row['ip_counter'];
				$ip_date = $row['ip_date'];
				$ip_status = $row['ip_status'];
				$ip_page_visit = $row['ip_page_visit'];
				$text = "<p class='ip-format' id='texttocopy_".$id_check."'>" .$ip_number. "</p>";
				$element = "'#texttocopy_".$id_check."'";

				$confirmar = '<div class=\"text-center\"><input type=\"text\" id=\"check_'.$id_check.'\" name=\"check_'.$id_check.'\" value=\"'.$ip_number.'\" hidden><button  onclick=\"checkIP('.$element.')\" data-toggle=\"tooltip\" data-placement=\"top\" data-id=\"'.$id_check.'\" data-ip-number=\"'.$ip_number.'\" data-ip-counter=\"'.$ip_counter.'\" data-ip-date=\"'.$ip_date.'\" data-ip-status=\"'.$ip_status.'\" data-ip-page-visit=\"'.$ip_page_visit.'\" title=\"Copiar IP\" class=\"btn btn-success btn-circle text-center text-white\"><i class=\"fa fa-save\" aria-hidden=\"true\"></i></button></div>';
				
				if($ip_status==1){
					$ip_status = "<b style='color:blue;'>Pendiente</b>";
				}else if($status==2){
					$ip_status = "<b style='color:red;'>Verificada</b>";
				}

				$tabla.='{
					"id_check":"'.$id_check.'",
					"ip_number":"'.$text.'",
					"ip_counter":"'.$ip_counter.'",
					"ip_date":"'.$ip_date.'",
					"ip_page_visit":"'.$ip_page_visit.'",
					"ip_status":"'.$ip_status.'",
					"acciones":"'.$confirmar.'"
				},';	

			}	
			//eliminamos la coma que sobra
			$tabla = substr($tabla,0, strlen($tabla) - 1);
			echo '{"data":['.$tabla.']}';	

            break;

    }


}
		
?>