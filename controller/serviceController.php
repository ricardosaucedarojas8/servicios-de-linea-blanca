<?php



if(isset($_POST['function']) && !empty($_POST['function'])) {

	$function = $_POST['function'];
    
    
	//En función del parámetro que nos llegue ejecutamos una función u otra
    switch($function) {

        case 'getServices': 
        	
        	require_once 'conn/connection.php';
	        
            $connect = new connection();
			$connection=$connect->connections();

			 $sql = "SELECT p.nombre_completo, p.direccion, p.cp, p.telefono, p.correo, e.nombre, m.nombre_marca, p.fecha_de_visita, p.hora_de_visita, p.diagnostico, s.id_servicio, m.img_marca, s.id_prospecto, s.status, s.fecha, s.folio_servicio, s.precio, emp.nombre as nombreEmp, emp.apellidos as nombreApp, s.id_empleado, p.empresa, p.id_equipo, p.id_marca, s.total_refacciones, s.comision  FROM servicios s INNER JOIN empleado emp ON s.id_empleado = emp.id_empleado INNER JOIN prospectos p ON p.id_prospecto = s.id_prospecto INNER JOIN marca m ON p.id_marca = m.id_marca INNER JOIN equipo e ON p.id_equipo = e.id_equipo WHERE s.status =1 OR s.status =2 OR s.status = 3 OR s.status = 4 ORDER BY s.id_servicio ASC";

			$result = mysqli_query($connection, $sql);

			$tabla = "";
			
			while($row = mysqli_fetch_array($result)){

				$id = $row['id_servicio'];
				$id_prospecto = $row['id_prospecto'];
				$folio = $row['folio_servicio'];
				$nombre = $row['nombre_completo'];
				$telefono = $row['telefono'];
				$fecha = $row['fecha'];
				$fecha_visita = $row['fecha_de_visita'];
				$correo = $row['correo'];
				$direccion = $row['direccion'];
				$id_marca = $row['id_marca'];
				$id_equipo = $row['id_equipo'];
				$marca = $row['nombre_marca'];
				$equipo = $row['nombre'];
				$precio = $row['precio'];
				$status = $row['status'];
				$hora_de_visita = $row['hora_de_visita'];
				$empresa = $row['empresa'];
				$cp = $row['cp'];
				$total_refacciones = $row['total_refacciones'];
				$antecedente = str_replace('.', ',', $row['diagnostico']);
				$comision = $row['comision'];

				//print_r($ante);

				$nombre_completo = $row['nombreEmp'] .' '. $row['nombreApp'] ;
				$llamada = '<a href=\"tel:+'.$telefono.'\" title=\"Llamar\" class=\"text-primary text-center\"> '.$telefono.'  </a>';
				$whatsapp = '<a href=\"https://wa.me/52'.$telefono.'\" title=\"whatsapp\" class=\"text-success text-center\"> '.$telefono.'  </a>';
				$contacto = $nombre .'<br>'. $llamada .' - ' . $whatsapp;

				$id_empleado = $row['id_empleado'];

				$cambiarMarca = '<a onclick=\"updateMarc(this)\" data-toggle=\"tooltip\" style=\"color:white !important;\" data-placement=\"top\" data-id=\"'.$id.'\" data-id-prospecto=\"'.$id_prospecto.'\" data-id-marca=\"'.$id_marca.'\" data-marca=\"'.$marca.'\" title=\"Cambiar Marca\" class=\"btn btn-success btn-circle text-center\"><i class=\"fas fa-sync\" aria-hidden=\"true\"></i></a>';


				$editar = '<a onclick=\"edithTechnic(this)\" data-toggle=\"tooltip\" style=\"color:white !important;\" data-placement=\"top\" data-id=\"'.$id.'\" data-id-prospecto=\"'.$id_prospecto.'\" data-id-empleado=\"'.$id_empleado.'\"  data-nombre-empleado=\"'.$nombre_completo.'\" title=\"Reasignar Técnico\" class=\"btn btn-info btn-circle text-center\"><i class=\"fa fa-users\" aria-hidden=\"true\"></i></a>';

				$verInfo = '<a onclick=\"showInfoService(this)\" data-toggle=\"tooltip\" style=\"color:white !important;\" data-placement=\"top\"  data-equipo=\"'.$equipo.'\" data-name=\"'.$nombre.'\" data-direc=\"'.$direccion.'\" data-correo=\"'.$correo.'\" data-marca=\"'.$marca.'\"  data-nombre-empleado=\"'.$nombre_completo.'\" data-antecedente=\"'.$antecedente.'\" data-fcreacion=\"'.$fecha.'\" data-cp=\"'.$cp.'\" title=\"Información del Servicio\" class=\"btn btn-primary btn-circle text-center\"><i class=\"fa fa-eye\" aria-hidden=\"true\"></i></a>';

				/*$crearGarantia = '<a onclick=\"addGuarantee(this)\" data-toggle=\"tooltip\" style=\"color:white !important;\" data-placement=\"top\" data-id=\"'.$id.'\" data-id-prospecto=\"'.$id_prospecto.'\" data-id-empleado=\"'.$id_empleado.'\" data-equipo=\"'.$equipo.'\" data-name=\"'.$nombre.'\" data-direc=\"'.$direccion.'\" data-correo=\"'.$correo.'\" data-marca=\"'.$marca.'\"  data-nombre-empleado=\"'.$nombre_completo.'\" title=\"Activar Garantia\" class=\"btn btn-warning btn-circle text-center\"><i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i></a>';*/



				$empleadoAsignado = '<b>' . $nombre_completo . '</b><br>' .$equipo. ' (<b class=\"text-warning\">' .$marca. '</b>)' ;


				if($status==1){
					$status = "<b style='color:blue;'>En proceso</b>";
				}else if($status==2){
					$status = "<b style='color:red;'>Pendiente</b>";
				}else if($status==3){
					$status = "<b style='color:green;'>Finalizado</b>";
				}else if($status==4){
					$status = "<b style='color:orange;'>Volver a Llamar</b>";
				}

				$folioStatus = '<b>p' . $id_prospecto . '</b><br>'. $status ;

				$tabla.='{
					"id":"'.$id.'",
					"id_prospecto":"'.$folioStatus.'",
					"contacto":"'.$contacto.'",
					"asignado":"'.$empleadoAsignado.'",
					"fecha":"'.$fecha.'",
					"fecha_visita":"<b>'.$fecha_visita.' - '.$hora_de_visita.'hrs</b>",
					"precio":"$'.$precio.'",
					"total_refacciones":"$'.$total_refacciones.'",
					"comision":"$'.$comision.'",
					"acciones":"'.$cambiarMarca.' '.$editar.' '. $verInfo.'"
				},';	

			}	
			//eliminamos la coma que sobra
			$tabla = substr($tabla,0, strlen($tabla) - 1);
			echo '{"data":['.$tabla.']}';	

            break;

         case 'getServicesLocation': 
        	
        	require_once 'conn/connection.php';
	        
            $connect = new connection();
			$connection=$connect->connections();

			 $sql = "SELECT p.nombre_completo, p.direccion, p.cp, p.telefono, p.correo, e.nombre, m.nombre_marca, p.fecha_de_visita, p.hora_de_visita, p.diagnostico, s.id_servicio, m.img_marca, s.id_prospecto, s.status, s.fecha, s.folio_servicio, s.precio, emp.nombre as nombreEmp, emp.apellidos as nombreApp, s.id_empleado, p.empresa, p.id_equipo, p.id_marca, s.total_refacciones, suc.suc_nombre FROM servicios s INNER JOIN empleado emp ON s.id_empleado = emp.id_empleado INNER JOIN prospectos p ON p.id_prospecto = s.id_prospecto INNER JOIN sucursales suc ON suc.id_sucursal = p.id_sucursal INNER JOIN marca m ON p.id_marca = m.id_marca INNER JOIN equipo e ON p.id_equipo = e.id_equipo WHERE s.status =1 OR s.status =2 OR s.status = 3 OR s.status = 4 ORDER BY s.id_servicio ASC";

			$result = mysqli_query($connection, $sql);

			$tabla = "";
			
			while($row = mysqli_fetch_array($result)){

				$id = $row['id_servicio'];
				$id_prospecto = $row['id_prospecto'];
				$suc_nombre = "<b>Sucursal:</b> " . $row['suc_nombre'];
				$folio = $row['folio_servicio'];
				$nombre = $row['nombre_completo'];
				$telefono = $row['telefono'];
				$datetime = $row['fecha'];
				$time = new DateTime($datetime);
				
				$horario = $time->format('H:i');
				$fecha = $date = $time->format('j.n.Y') . " <br>" . $horario ;

				$fecha_visita = $row['fecha_de_visita'];
				$correo = $row['correo'];
				$direccion = $row['direccion'];
				$id_marca = $row['id_marca'];
				$id_equipo = $row['id_equipo'];
				$marca = $row['nombre_marca'];
				$equipo = $row['nombre'];
				$precio = $row['precio'];
				$status = $row['status'];
				$hora_de_visita = $row['hora_de_visita'];
				$empresa = $row['empresa'];
				$cp = "<b>CP:</b> ".$row['cp'];
				$total_refacciones = $row['total_refacciones'];
				$antecedente = str_replace('.', ',', $row['diagnostico']);

				//print_r($ante);

				$nombre_completo = $row['nombreEmp'] .' '. $row['nombreApp'] ;
				$contacto = $nombre;

				$id_empleado = $row['id_empleado'];

				$empleadoAsignado = '<b>' . $nombre_completo . '</b><br>' .$equipo. ' (<b class=\"text-warning\">' .$marca. '</b>)' ;


				if($status==1){
					$status = "<b style='color:blue;'>En proceso</b>";
				}else if($status==2){
					$status = "<b style='color:red;'>Pendiente</b>";
				}else if($status==3){
					$status = "<b style='color:green;'>Finalizado</b>";
				}else if($status==4){
					$status = "<b style='color:orange;'>Volver a Llamar</b>";
				}

				$folioStatus = '<b>p' . $id_prospecto . '</b><br>'. $status ;

				$tabla.='{
					"id":"'.$id.'",
					"id_prospecto":"'.$folioStatus.'",
					"contacto":"'.$contacto.'",
					"direccion": "'.$suc_nombre. " " . $cp . "<br> <span class='tdScroll'> <b>Dirección:</b> ".$direccion. "</span>" . '",
					"asignado":"'.$empleadoAsignado.'",
					"fecha":"'.$fecha.'",
					"precio":"$'.$precio.'",
					"total_refacciones":"$'.$total_refacciones.'"
				},';	

			}	
			//eliminamos la coma que sobra
			$tabla = substr($tabla,0, strlen($tabla) - 1);
			echo '{"data":['.$tabla.']}';	

            break;

        case 'addService':
        	
        	date_default_timezone_set('America/Mexico_City');
        	
        	# code...
        	$id_prospecto = $_POST['id_prospecto'];
        	$id_empleado = $_POST['id_empleado'];
        	$status = $_POST['status'];
        	$id_tipo_servicio = 0;
        	$tiempo_de_entrega = date("Y-m-d");
        	$fecha = date('Y-m-d H:i:s');;
        	$anticipo = 0;
        	$diagnostico = "";
        	$folio_servicio = "";
        	$folio_garantia = "";
        	$entrada = date("Y-m-d");
        	$comentarios = "";
        	$salida = date("Y-m-d");
        	$garantia = "1";
        	$calificacion = 0;
        	$precio = 0;
        	$id_metodo_pago = 0;
        	$factura = 0;
        	$total_refacciones=0;
        	$comision = 0;

        	require_once 'conn/connection.php';
        	$connect = new connection();
			$connection=$connect->connections();
			
			
			$sql = "INSERT INTO servicios (id_empleado, id_prospecto, id_tipo_servicio, id_metodo_pago, diagnostico, precio, fecha_entrega, anticipo, entrada, comentarios, salida, garantia, status, fecha, calificacion, status_factura, folio_servicio, folio_garantia, total_refacciones, comision) VALUES ('$id_empleado','$id_prospecto','$id_tipo_servicio','$id_metodo_pago','$diagnostico','$precio','$tiempo_de_entrega ', '$anticipo', '$entrada', '$comentarios', '$salida', '$garantia', '$status', '$fecha', '$calificacion','$factura', '$folio_servicio', '$folio_garantia', '$total_refacciones', '$comision');";
			

			$select = "SELECT correo FROM empleado WHERE id_empleado = '$id_empleado'";

			$result = mysqli_query($connection, $select);
			
			// Variable $row hold the result of the query
			$row = mysqli_fetch_assoc($result);

			$correo = $row['correo'];

			$para      = $correo;

			// To send HTML mail, the Content-type header must be set
			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
			$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			 
			// Create email headers
			$cabeceras .= 'From: contacto@reparacionparaestufas.com  '."\r\n".
			    'Reply-To: contacto@reparacionparaestufas.com '. "\r\n" .
			    'X-Mailer: PHP/' . phpversion();

			$titulo    = 'Nuevo Servicio';

			$mensaje = '
			<html>
		        <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		        
		            <title>Nuevo Servicio</title>
		        </head>
		        <body>
		            <div>
		            <h1><b>Nuevo Servicio </b></h1>
		            <p>Tienes un nuevo servicio en tu bandeja de entrada, verificalo en el sistema: <b><a href="http://platform.reparacionparaestufas.com/views/technics">http://platform.reparacionparaestufas.com/views/technics</a></b></p>
		            </div>

		            <footer class="my-5 pt-5 text-muted text-center text-small">
                    <p class="mb-1">&copy; 2015-2021 | SIHOGARCDMX</p>
                </footer>
		        </body>
		    </html>';

			

			$success = mail($para, $titulo, $mensaje, $cabeceras);

			if (!$success) {
			    $errorMessage = error_get_last()['message'];
			}

			$jsondata = array();


        	if ($connection->query($sql)===true) {

        		$updt = "UPDATE prospectos SET status=2 WHERE id_prospecto=".$id_prospecto;

				if ($connection->query($updt) === TRUE) {
					$message= "El Servicio se ha asignado a un técnico con éxito.";
				    $jsondata['success'] = true;
	        		$jsondata['message'] = $message;
        		
				} else {
					print_r("Error updating record: " . $connection->error);
				}

        		

			} else {
				$message= "El Servicio no ha sido asignado";
				print_r("Error record: " . $connection->error);
			    $jsondata['success'] = false;
        		$jsondata['message'] = $message;
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);


        	break;

        case 'serviceConfirm':
        	# code...
        	require_once 'conn/connection.php';
			$connect = new connection();
			$connection=$connect->connections();

			$id_metodo_pago = $_POST['id_metodo_pago'];
			$correo = $_POST['correo'];
			$equipo = $_POST['equipo'];
			$anticipo = $_POST['anticipo'];
			$total_refacciones = $_POST['total_refacciones'];
			$marca = $_POST['marca'];
			$nombre = $_POST['nombre'];
			$factura = $_POST['factura'];
			$id_servicio = $_POST['id_servicio'];
			$id_empleado = $_POST['id_empleado'];
			$folio_garantia = $_POST['folio_garantia'];
			$folio = $_POST['folio'];
        	$fecha_entrega = $_POST['fecha_entrega'];
        	$id_tipo_servicio = $_POST['id_tipo_servicio'];
        	$diagnostico_final = $_POST['diagnostico_final'];
        	$costo = $_POST['costo'];
        	$comision = $_POST['comision'];
        	$status = $_POST['status'];
			
			$sql = "UPDATE servicios SET id_metodo_pago='$id_metodo_pago', id_tipo_servicio = '$id_tipo_servicio', folio_garantia='$folio_garantia', folio_servicio='$folio', fecha_entrega='$fecha_entrega', diagnostico='$diagnostico_final', precio='$costo', status_factura='$factura', status='$status', anticipo='$anticipo', total_refacciones='$total_refacciones', comision='$comision' WHERE id_servicio='$id_servicio'";


			$select = "SELECT ts_nombre FROM tipo_servicio WHERE id_tipo_servicio = '$id_tipo_servicio'";

			$result = mysqli_query($connection, $select);
			
			// Variable $row hold the result of the query
			$row = mysqli_fetch_assoc($result);

			$servicio = $row['ts_nombre'];


			$para      = $correo;

			// To send HTML mail, the Content-type header must be set
			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
			$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			 
			// Create email headers
			$cabeceras .= 'From: contacto@reparacionparaestufas.com  '."\r\n".
			    'Reply-To: contacto@reparacionparaestufas.com '. "\r\n" .
			    'X-Mailer: PHP/' . phpversion();

			$titulo    = 'Home Service '.$marca.' | Gracias por confiar en nosotros.';

			if($status == "4"){

				$mensaje = '
			<html>
		        <head>
		        
		            <title>Revisión Finalizada con Éxito</title>
		        </head>
		        <body>
		            <div>
		            <h1><b>Revisión Finalizada con Éxito</b></h1>
		            <p>Esperamos vuelvas pronto '.$nombre.', que tengas buen día. Recuerde regalarnos una recomendación en nuestra página de Facebook: <b><a href="https://www.facebook.com/sihogarcdmx/">https://www.facebook.com/sihogarcdmx</a></b></p>
		            </div>

		            <footer class="my-5 pt-5 text-muted text-center text-small">
                    <p class="mb-1">&copy; 2015-2021 | SIHOGARCDMX</p>
                </footer>
		        </body>
		    </html>';

			

			}else if($status == "3"){

				$mensaje = '
			<html>
		        <head>
		        
		            <title>Servicio Finalizado con Éxito</title>
		        </head>
		        <body>
		            <div>
		            <h1><b>Servicio Finalizado con Éxito</b></h1>
		            <p>Gracias por confiar en nosotros, por este medio confirmamos el termino de su servicio Sr/a: <b>'.$nombre.'</b><br><br>

		            	<b>Folio del servicio:</b> '.$folio.' <br>
		            	Servicio: <b>'.$servicio.'</b><br>
		            	Equipo: <b>'.$equipo.'</b><br>
		            	Marca: <b>'.$marca.'</b><br> 
		            	Finalizado en la fecha: <b>'.$fecha_entrega . '</b> del año en curso. <br><br>Recuerde regalarnos una recomendación en nuestra página de Facebook: <b><a href="https://www.facebook.com/sihogarcdmx">https://www.facebook.com/sihogarcdmx</a></b></p>
		            </div>
		            <footer class="my-5 pt-5 text-muted text-center text-small">
                    <p class="mb-1">&copy; 2015-2021 | SIHOGARCDMX</p>
                </footer>
		        </body>
		    </html>';

			}else if($status == "2"){
				
				$mensaje = '
			<html>
		        <head>
		        
		            <title>Anticipo del Servicio</title>
		        </head>
		        <body>
		            <div>
		            <h1><b>Bienvenido a Home Service</b></h1>
		            <p>Hola <b>'.$nombre.'</b>, esperamos quedes satisfecho con nuestro servicios. Recuerde regalarnos una recomendación en nuestra página de Facebook: <b><a href="https://www.facebook.com/sihogarcdmx/">https://www.facebook.com/sihogarcdmx</a></b></p>
		            </div>
		            <footer class="my-5 pt-5 text-muted text-center text-small">
                    <p class="mb-1">&copy; 2015-2021 | SIHOGARCDMX</p>
                </footer>
		        </body>
		    </html>';


			}


			$success = mail($para, $titulo, $mensaje, $cabeceras);

			if (!$success) {
			    $errorMessage = error_get_last()['message'];
			}


        	$jsondata = array();

        	if ($connection->query($sql)===true) {
			    $jsondata['success'] = true;
			    $jsondata['id_empleado'] = $id_empleado;
        		$jsondata['message'] = 'Felicidades! Has actualizado este servicio.';

			} else {
			    $jsondata['success'] = false;
        		$jsondata['message'] = 'Error! Ha ocurrido un error, avisa a un administrador.';
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);

        	break;

        case 'endService':
        	# code...
	        require_once 'conn/connection.php';
			
			$id_servicio = $_POST['id_servicio'];
			$correo = $_POST['correo'];
			$equipo = $_POST['equipo'];
			$marca = $_POST['marca'];
			$nombre_completo = $_POST['nombre_completo'];
			$fecha_entrega = $_POST['fecha_entrega'];
			$id_tipo_servicio = $_POST['id_tipo_servicio'];
			$folio = $_POST['folio'];
			$folio_garantia = $_POST['folio_garantia'];
			$refacciones = $_POST['refacciones'];

			$connect = new connection();
			$connection=$connect->connections();
			$sql = "UPDATE servicios SET status = 3, total_refacciones = '$refacciones' WHERE id_servicio = " . $id_servicio .";";

			$select = "SELECT ts_nombre FROM tipo_servicio WHERE id_tipo_servicio = '$id_tipo_servicio'";

			$result = mysqli_query($connection, $select);
			
			// Variable $row hold the result of the query
			$row = mysqli_fetch_assoc($result);

			$servicio = $row['ts_nombre'];
        	
        	$jsondata = array();

        	if ($connection->query($sql)===true) {

        		$para      = $correo;

				// To send HTML mail, the Content-type header must be set
				$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
				$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 
				// Create email headers
				$cabeceras .= 'From: contacto@reparacionparaestufas.com  '."\r\n".
				    'Reply-To: contacto@reparacionparaestufas.com '. "\r\n" .
				    'X-Mailer: PHP/' . phpversion();

				$titulo    = 'Home Service | Gracias por confiar en nosotros.';

				/**$mensaje = '
					<html>
				        <head>
				        
				            <title>Servicio Finalizado</title>
				        </head>
				        <body>
				            <div>
				            <h1><b>Servicio Finalizado</b></h1>
				            <p>Hola <b>apreciable cliente</b>, esperamos hayas quedado satisfecho/a con nuestros servicios. Recuerda regalarnos una recomendación en nuestra página de Facebook: <b><a href="https://www.facebook.com/sihogarcdmx/">https://www.facebook.com/sihogarcdmx</a></b></p>
				            </div>
				            <footer class="my-5 pt-5 text-muted text-center text-small">
		                    <p class="mb-1">&copy; 2015-2020 | SIHOGARCDMX</p>
		                </footer>
				        </body>
				    </html>';**/

				    $mensaje = '
						<html>
					        <head>
					        
					            <title>Servicio Finalizado con Éxito</title>
					        </head>
					        <body>
					            <div>
					            <h1><b>Servicio Finalizado con Éxito</b></h1>
					            <p>Gracias por confiar en nosotros, por este medio confirmamos el termino de su servicio Sr/a: <b>'.$nombre_completo.'</b><br><br>

					            	<b>Folio del servicio:</b> '.$folio.' <br>
					            	Servicio: <b>'.$servicio.'</b><br>
					            	Equipo: <b>'.$equipo.'</b><br>
					            	Marca: <b>'.$marca.'</b><br> 
					            	Finalizado en la fecha: <b>'.$fecha_entrega . '</b> del año en curso. <br><br>Recuerde regalarnos una recomendación en nuestra página de Facebook: <b><a href="https://www.facebook.com/sihogarcdmx/">https://www.facebook.com/sihogarcdmx/</a></b></p>
					            </div>
					            <footer class="my-5 pt-5 text-muted text-center text-small">
			                    <p class="mb-1">&copy; 2015-2021 | SIHOGARCDMX</p>
			                </footer>
					        </body>
					    </html>';

				$success = mail($para, $titulo, $mensaje, $cabeceras);

				if (!$success) {
				    $errorMessage = error_get_last()['message'];
				}

			    $jsondata['success'] = true;
        		$jsondata['message'] = 'Felicidades! Has Terminado este servicio con éxito.';

			} else {
			    $jsondata['success'] = false;
        		$jsondata['message'] = 'Error! Ha ocurrido un error, avisa a un administrador.';
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);

			break;

		case 'edithTechnic':

			require_once 'conn/connection.php';
			
			$id_servicio = $_POST['id_servicio'];
			$id_empleado = $_POST['id_empleado'];

			$connect = new connection();
			$connection=$connect->connections();
			$sql = "UPDATE servicios SET id_empleado = " . $id_empleado ." WHERE id_servicio = " . $id_servicio .";";

			$jsondata = array();

        	if ($connection->query($sql)===true) {

        	$jsondata['success'] = true;
        		$jsondata['message'] = 'Felicidades! Has Reasignado el Técnico a este servicio con éxito.';

			} else {
			    $jsondata['success'] = false;
        		$jsondata['message'] = 'Error! Ha ocurrido un error, avisa a un administrador.';
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);

			break;

		case 'edithMarc':

			require_once 'conn/connection.php';
			
			$id_prospecto = $_POST['id_prospecto'];
			$id_marca = $_POST['id_marca'];

			$connect = new connection();
			$connection=$connect->connections();
			$sql = "UPDATE prospectos SET id_marca = " . $id_marca ." WHERE id_prospecto = " . $id_prospecto .";";

			$jsondata = array();

        	if ($connection->query($sql)===true) {

        	$jsondata['success'] = true;
        		$jsondata['message'] = 'Felicidades! Has Actualizado la marca en este servicio con éxito.';

			} else {
			    $jsondata['success'] = false;
        		$jsondata['message'] = 'Error! Ha ocurrido un error, avisa a un administrador.';
			}
			//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
		    header('Content-type: application/json; charset=utf-8');
		    
		    echo json_encode($jsondata, JSON_FORCE_OBJECT);

			break;

    }


}
		
?>