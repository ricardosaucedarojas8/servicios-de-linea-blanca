//SHOW MODAL TO EDITH
function showDetails(data) {
  
  $("#saveButton").hide();
  $("#edithButton").show();

  document.getElementById("modalTitle").innerHTML = "<i class='fas fa-edit color-edith'></i> Edith Product"; 
  
  var id_servicio = $(data).attr("data-id");
  var nombre = $(data).attr("data-name");
  var desc = $(data).attr("data-desc");
  var precio = $(data).attr("data-cost");
  var id_tipo_servicio = $(data).attr("data-id-tipo");
  var img_url = $(data).attr("data-url");

  $('#img-gral').attr('src', img_url);

  $('#id').val(id_servicio);
  $('#name').val(nombre);
  $('#desc').val(desc);
  $('#cost').val(precio);
  //$('#img_url').val(img_url);
  $('#url_edith').val(img_url);
  $('#id_tipo_servicio').val(id_tipo_servicio);

  $(document).ready(function(){
    $("#myModal").modal();
  });
}

//SHOW THE MODAL TO INSERT
function resetModal(){
    $('#myForm').get(0).reset();
    document.getElementById('edithButton').style.display="none";
    document.getElementById('saveButton').style.display="block";
    document.getElementById("modalTitle").innerHTML = "<i class='fas fa-plus-circle color-success'></i> Add Service"; 
    $('#id').hide();
    $('#lab-id').hide();
    $(".modal-body input").val("");
    $("#myModal").modal();
    $('#img-gral').attr('src', '../img/img-services/img-service-2.jpg');
}

//CLOSE THE MODAL AND DO INSERT OR UPDATE
function doAction(){

  //var id_servicio = document.getElementById("id").value;
  //var name = document.getElementById("name").value;
  //var desc = document.getElementById("desc").value;
  //var cost = document.getElementById("cost").value;

  var id_servicio = $('#id')[0].value;
  var check_url_edith = $('#url_edith')[0].value;
  var id_tipo_servicio = $('#id_tipo_servicio')[0].value;
  var name = $('#name')[0].value;
  var desc = $('#desc')[0].value;
  var cost = $('#cost')[0].value;
  var img_url = $('#img_url').prop('files')[0];
  
  if (typeof img_url === 'undefined'){
     img_url = check_url_edith;
  }

  //validar si viene vacio ES INSERT, sino ES UPDATE

  if(id_servicio === ""){
    //insert
    console.log("INSERT - nombre: " + name + "desc: " + desc + "precio: " + cost + "id_tipo_servicio " + id_tipo_servicio);

    if(name === ""){ 
      $( "#name" ).focus();
      //document.getElementById("errorName").innerHTML = "Service Name is Required";
      $("#errorName").show();
      $("#errorName").html("Service Name is Required");
    }else if(id_tipo_servicio === "" || id_tipo_servicio === "0"){
      $( "#id_tipo_servicio" ).focus();
      //document.getElementById("errorName").innerHTML = "Service Name is Required";
      $("#errorName").hide();
      $("#errorSelect").show();
      $("#errorSelect").html("Service Type is Required");
    }else if(desc === "" || desc === "0"){
      $( "#desc" ).focus();
      $("#errorSelect").hide();
      $("#errorDesc").show();
      //document.getElementById("errorDesc").innerHTML = "Service Description is Required";
       $("#errorDesc").html("Service Name is Required");
    }else if(cost === "" || cost === "0"){
      $( "#cost" ).focus();
      $("#errorDesc").hide();
      $("#errorCost").show();
      //document.getElementById("errorCost").innerHTML = "Service Cost is Required";
      $("#errorCost").html("Service Cost is Required");

    }else{
      
      var formData = new FormData();

      formData.append('name',name);
      formData.append('id_tipo_servicio',id_tipo_servicio);
      formData.append('desc',desc);
      formData.append('cost',cost);
      formData.append('archivo',img_url);
      formData.append('function',"addService");

      $.ajax({
        data:  formData, //send data via AJAX
        url:   '../controller/serviceController.php', //url file controller PHP
        dataType: 'json', // what to expect back from the server
        cache: false,
        contentType: false,
        processData: false,
        type:  'post', //send POST data
        beforeSend: function () {
          //document.getElementById("load").style.display = "block";
          $("#load").show();
        },
        success:  function (response) { //get request
         
        if(response.success){ 
          $("#successModalTitle").html("<i class='fas fa-check-circle color-success'></i> ¡Success!");
          $("#successModalDescription").html(response.message);
        }else{
          $("#successModalTitle").html("<i class='fas fa-exclamation-circle color-error'></i> ¡Error!");
          $("#successModalDescription").html(response.message);
        }
          
          $("#load").hide();
          //alert(response.message);
          $('#actionModal').modal('toggle');
          $('#services').DataTable().ajax.reload();
          $('.close').click(); 
          
        }
      });
    }
    //$('#myModal').modal('toggle');

  }else{
    //update
    console.log("UPDATE - nombre: " + name + "desc: " + desc + "precio: " + cost + "id_tipo_servicio " + id_tipo_servicio)

    var formData = new FormData();

    formData.append('name',name);
    formData.append('id',id_servicio);
    formData.append('id_tipo_servicio',id_tipo_servicio);
    formData.append('desc',desc);
    formData.append('cost',cost);
    formData.append('archivo',img_url);
    formData.append('function',"edithService");

  $.ajax({
    data:  formData, //send data via AJAX
    dataType: 'json', // what to expect back from the server
    cache: false,
    contentType: false,
    processData: false,
    url:   '../controller/serviceController.php', //url file controller PHP
    type:  'post', //send POST data
    beforeSend: function () {
      $("#resultado").html("Procesando, espere por favor...");
    },
    success:  function (response) { //get request
      
    if(response.success){ 
      $("#successModalTitle").html("<i class='fas fa-check-circle color-success'></i> ¡Success!");
    }else{
      $("#successModalTitle").html("<i class='fas fa-exclamation-circle color-error'></i> ¡Error!");
    }
      $("#successModalDescription").html(response.message);  
          //alert(response.message);
      $('#actionModal').modal('toggle');
      $('#services').DataTable().ajax.reload();
      $('.close').click(); 
          
    }
  });

  }

}



function showDeleteService(){
  $(document).ready(function(){
    $("#deleteModal").modal();
  });
}


function deleteService(){
  var id = document.getElementById("id_confirm").value;
  var parametros = {
    "id" : id,
    "function" : "deleteService"
  };
  $.ajax({
    data:  parametros, //send data via AJAX
    url:   '../controller/serviceController.php', //url file controller PHP
    type:  'post', //send POST data
    beforeSend: function () {
      $("#resultado").html("Procesando, espere por favor...");
    },
    success:  function (response) { //get request
      $("#successModalTitle").html("<i class='fas fa-check-circle color-success'></i> ¡Success!");
      $("#successModalDescription").html("The Record was sucessfully deleted");
      //alert(response.message);
      $('#actionModal').modal('toggle');
      $('#services').DataTable().ajax.reload();
      $('.close').click(); 
      
    }
  });
}

function showConfirm(data){
  $("#confirmTitle").html("<i class='fas fa-exclamation-triangle color-confirm'></i> ¡Confirm!");
  $("#confirmDescription").html("Do you want to delete this record?");
  $('#confirmActionModal').modal('toggle');
  var id_servicio = $(data).attr("data-id");
  $('#id_confirm').val(id_servicio);
}

function confirm(){
  $('.close').click(); 
}

/*
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function limpia() {
    var name = $('#name')[0].value;
    var desc = $('#desc')[0].value;
    var tam_name = name.length;
    var tam_desc = desc.length;
    for(i = 0; i < tam_name; i++) {
        if(!isNaN(name[i]))
            $('#name').val("");
    }

    for(i = 0; i < tam_desc; i++) {
        if(!isNaN(desc[i]))
              $('#desc').val("");
    }
}
*/

