<?php 
	/**
	 * 
	 */
	class UserModel {
		
		public $id_user = '';
		public $usr_correo = '';
		public $usr_password = '';
 
		
		public function __construct($id_user, $usr_correo, $usr_password) {
	       $this->id_user = $id_user;
	       $this->usr_correo = $usr_correo;
	       $this->usr_password = $usr_password;
	    }

		public function getIdUsuario(){
			return $this->id_user;
		}

		public function setIdUsuario($id_user){
			$this->id_user = $id_user;
		}

		public function getUsrCorreo(){
			return $this->usr_correo;
		}

		public function setUsrCorreo($usr_correo){
			$this->usr_correo = $usr_correo;
		}

		public function getUsrPassword(){
			return $this->usr_password;
		}

		public function setSUsrPassword($usr_password){
			$this->usr_password = $usr_password;
		}

	}

?>
