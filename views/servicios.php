<?php 
$id_prospecto;
$id_usuario;


if(isset($_GET['pros']) && !empty($_GET['pros'])){
  
  $id_prospecto = $_GET['pros'];
  $id_usuario = $_GET['idu'];
}else{
  $id_prospecto = false;
  $id_usuario = false;
}

function getEmpleados(){
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM empleado WHERE status = 1 ORDER BY id_empleado";
  
  $result = mysqli_query($connection, $sql);
  $tabla = "";
  
  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_empleado'].'"> '.$row['nombre'].' '.$row['apellidos'].'</option>';    
  }
}

function getProspecto($id_prospecto){ 

  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  

  $sql = "SELECT p.nombre_completo, p.direccion, p.telefono, p.correo, e.nombre, m.nombre_marca, p.fecha_de_visita, p.hora_de_visita, p.diagnostico, p.cp FROM prospectos p, marca m, equipo e WHERE p.id_prospecto = $id_prospecto and p.id_marca = m.id_marca and p.id_equipo = e.id_equipo";
  
  $result = mysqli_query($connection, $sql);

  /* fetch associative array */
  while ($row = mysqli_fetch_row($result)) {
    
    $direccion = $row[1];
    
    echo'<div class="col-md-12"> <h2>Información del Servicio</h2>  <hr></div>';
    echo'<div class="col-md-4">';
    echo'<p><b>Cliente</b>: '.$row[0].'<br><b>Teléfono</b>: '.$row[2].' <br><b>Correo</b>: '.$row[3].'<br><b>Equipo: </b>'.$row[4].' (<b>'.$row[5].'</b>)</p>';
    echo'</div>';
    echo'<div class="col-md-8">';
    echo'<p><b>Fecha y Hora</b>: '.$row[6].'<b> - </b> '.$row[7].' hrs <br> <b>CP</b>: '.$row[9].' <b>Dirección</b>: '.$row[1].' <br><b>Pre-diagnostico</b>: '.$row[8].'</p>';
    echo'</div>';
  }

}

?>
<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <!-- Custom fonts for this template-->
  
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  <script src="../js/servicio.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
  <title>Asignar Servicio</title>
</head>
<body class="bg-light">

  <div class="container">
    <div class="py-5 text-center">
      <img class="d-block mx-auto mb-4" src="../img/logotipo-morado.png" alt="" width="100" >
      <h2>Asignar Servicio</h2>
      <p class="lead">Asigna al nuevo servicio un técnico especialista para que lo atienda.</p>
    </div>

    <div class="row">     
     <?php getProspecto( $id_prospecto); ?>
   </div>

   <div class="row">
    <div class="col-md-12">
      <form id="form_service" class="needs-validation" novalidate>
        <input value="<?php echo $id_prospecto ?>"  type="hidden" id="id_prospecto">
        <input value="<?php echo $id_prospecto ?>"  type="hidden" id="correo">
        <h2>Asignar Técnico</h2>
        <select id="id_empleado" class="custom-select d-block w-100" required="">
          <option value="0">Selecciona un técnico...</option>
          <?php getEmpleados(); ?>
        </select>
        <div class="invalid-feedback" id="errorEmpleado">
          Please select an specialist.
        </div>
        <br>
        <a onclick="createService()" class="btn btn-add btn-lg btn-block">Asignar Servicio</a>
      </form>
    </div>
    
  </div>
</div>

<!-- SUCCESS / FAIL ACTION MODAL -->

<div class="modal fade" id="actionModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <!-- Modal Header -->
      <div class="modal-header">
        <h2 class="modal-title"><p id="successModalTitle"></p></h2>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form>
        <!-- Modal body -->
        <div class="modal-body">
          <p id="successModalDescription"></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" onclick="confirm(<?php echo''.$id_usuario; ?>)">Agree</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php include 'footer.php';?>

</div>

    <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <!-- Bootstrap core JavaScript-->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    </body>
    </html>

    <?php ?>

