<?php 
  date_default_timezone_set('America/Mexico_City');

  if(isset($_GET['suc']) && !empty($_GET['suc'])){
      
    $success = $_GET['suc'];
    $id_usuario = $_GET['idu'];
  }else{
    $success = false;
    $id_usuario = false;
  }

  function countProspectos($id_usuario){
    
    require_once '../controller/conn/connection.php';
    $connect = new connection();
    $connection=$connect->connections();

    $sql = "SELECT count(id_marca) as total FROM marca WHERE status = 1 ";

    $result = mysqli_query($connection, $sql);
    
    $data=mysqli_fetch_assoc($result);

    $total_pendientes = $data['total'];

    return $total_pendientes;
  }


  function getProspectos($id_usuario){ 
    require_once '../controller/conn/connection.php';
    $connect = new connection();
    $connection=$connect->connections();

    $sql = "SELECT * FROM marca WHERE status = 1  ORDER BY id_marca DESC";
    
    $result = mysqli_query($connection, $sql);
    $tabla = "";
        
    while($row = mysqli_fetch_array($result)){
      /*
      echo'<li class="list-group-item d-flex justify-content-between lh-condensed">';
        echo '<div>';
          echo'<small class="text-muted visit" style="display:inline-block;"><img src="../img/'.$row['img_marca'].'" width="50px" > </small><br>';
            echo'<small class="text-muted visit">'.$row['descripcion'].'</small><br>';
            echo'<small class="text-muted hour">'.$row['fecha'].' hrs</small><br>';
            echo'<a onclick="showProspect(this)" data-nombre-marca="'.$row['nombre_marca'].'" data-descripcion="'.$row['descripcion'].'" data-fecha="'.$row['fecha'].'" data-status="'.$row['status'].'"><small class="text-edith">Editar</small></a>';
            echo '<a  onclick="asignarServicio(this)" style="" data-nombre-marca="'.$row['nombre_marca'].'" data-descripcion="'.$row['descripcion'].'" data-fecha="'.$row['fecha'].'" data-status="'.$row['status'].'">Ver</a>';
        echo'</div>';

      echo'</li>';
    */
    echo '<div class="media">
  <img src="../img/'.$row['img_marca'].'"  class="mr-3 mobile-img" alt="...">
  <div class="media-body">
    <h5 class="mt-0">Media heading</h5>
    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
  </div>
</div><br>';


    }

}
?>
<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  <script src="../js/prospecto.js"></script>
  <title>Marcas</title>

  <script type="text/javascript">
    // Función de javascript para ejecutar repetidamente
    window.setInterval(
        function(){
        // Sección de código para modificar el DIV
        // $("#miDiv").text(variable);

        // Ejemplo: Cada dos segundos se imprime la hora
        $("#num_prospectos").load("../views/marcs.php?suc=true&idu=<?php echo''.$id_usuario; ?>  #num_prospectos");
        $("#propectos_refresh").load("../views/marcs.php?suc=true&idu=<?php echo''.$id_usuario; ?> #propectos_refresh");
      }
      // Intervalo de tiempo
    ,60000);
  </script>
  </head>

  <body class="bg-light">

    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="../img/logotipo-morado.png" alt="" width="100" >
        <h2>Marcas</h2>
        <p class="lead">Captura los datos que a continuación se requieren, todos los datos con * son obligatorios.</p>
      </div>


      <div class="row list-grid">
            <?php 
              getProspectos($id_usuario);
            ?>
      </div>
    
      <!-- Start Global Footer-->

        <?php require_once 'footer.php'; ?>

      <!-- End Global Footer-->

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- Bootstrap core JavaScript-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
  </body>
</html>

<?php ?>