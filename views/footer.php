<?php ?>



<div id="circularMenu" class="circular-menu">

  <a class="floating-btn" onclick="document.getElementById('circularMenu').classList.toggle('active');">
    <i class="fa fa-plus"></i>
  </a>

  <menu class="items-wrapper">

    <a class="menu-item" title="Prospectos" href="index.php?suc=false&idu=<?php echo''.$id_usuario?>"> <i class="fa fa-user-plus" aria-hidden="true"></i></a>

    <a class="menu-item" title="Servicios" href="gral_list.php?suc=false&idu=<?php echo''.$id_usuario?>"><i class="far fa-id-card" aria-hidden="true"></i></a> 

    <a class="menu-item" href="checkip.php?suc=false&idu=<?php echo''.$id_usuario?>" title="CheckIP"><i class="fa fa-eye" aria-hidden="true"></i></a>

    <a class="menu-item" href="marcs.php?suc=false&idu=<?php echo''.$id_usuario?>" title="Marcas"><i class="fa fa-database" aria-hidden="true"></i></a>

    <a class="menu-item" title="Cerrar Sesión" href="../"><i class="fas fa-sign-out-alt"></i></a>

    <a class="menu-item" href="marcs.php?suc=false&idu=<?php echo''.$id_usuario?>" title="Garantias"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></a>

    <a href="filter_branch.php?suc=false&idu=<?php echo''.$id_usuario?>" class="menu-item text-white" title="Seguimiento de Sucursales"><i class="fa fa-search" aria-hidden="true"></i></a>

    <a class="menu-item text-white " title="?"><i class="fa fa-question" aria-hidden="true"></i></a>

    <a class="menu-item text-white" title="?"><i class="fa fa-question" aria-hidden="true"></i></a>

  </menu>

</div>


<footer class="my-5 pt-5 text-muted text-center text-small">
	<p class="mb-1">&copy; 2015-2022 | Powered by PWP with ❤</p>

</footer>