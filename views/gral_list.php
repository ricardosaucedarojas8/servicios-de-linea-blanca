<?php 

$id_usuario;


if(isset($_GET['idu']) && !empty($_GET['idu'])){

  $id_usuario = $_GET['idu'];
}else{
  $id_usuario = false;
}

function getEmpleados(){
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM empleado WHERE status = 1 ORDER BY id_empleado";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_empleado'].'"> '.$row['nombre'].' '.$row['apellidos'].'</option>';    
  }
}

function getDestinos(){
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM cmp_destino WHERE status = 1 ORDER BY id_destino";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['nombre'].' - '.$row['gpo_de_anuncios'].'"> '.$row['nombre'].' | '.$row['gpo_de_anuncios'].'</option>';    
  }
}

function getMarca(){ 
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM marca WHERE status = 1 ORDER BY nombre_marca";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_marca'].'"> '.$row['nombre_marca'].'</option>';    
  }

}

function getEquipo(){ 
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM equipo WHERE status = 1 ORDER BY nombre";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_equipo'].'"> '.$row['nombre'].'</option>';    
  }

}


?>

<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <!-- Custom fonts for this template-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  <script src="../js/servicio.js"></script>
  <script src="../js/prospecto.js"></script>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">


  <title>Todos los Servicios</title>

</head>

<body class="bg-light">

  <div class="container-fluid">

    <div class="col-md-12">

      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="../img/logotipo-morado.png" alt="" width="100" >
        <h2>Todos los Servicios</h2>
        <p class="lead">A continuación se enlistan todos los servicios que han sido registrados en el sistema.</p>
      </div>

      <div class="row">
        <div class="table-responsive">
          <table id="services" class="display nowrap" style="width:100%">
            <thead>
              <tr>
                <th>ID</th>
                <th>Folio/Status</th>
                <th>Acciones</th>
                <th>Contacto</th>  
                <th>Asignado</th>        
                <th>Precio</th>
                <th>Comisión</th>
                <th>Refacciones</th>
                <th>FCreación</th>
                <th>Fecha y hora de la visita</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr>
               <th>ID</th>
               <th>Folio/Status</th>
               <th>Acciones</th>
               <th>Contacto</th>
               <th>Asignado</th>  
               <th>Precio</th>
               <th>Comisión</th>
               <th>Refacciones</th>
               <th>FCreación</th>
               <th>Fecha y hora de la visita</th>
             </tr>
           </tfoot>
         </table>
       </div>
     </div>
   </div>


  <!-- UPDATE MARC -->
  
  <div class="modal fade" id="updateMarc" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title">Actualizar Marca</h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
          <!-- Modal body -->
          <div class="modal-body">
            <p id="marcText"></p>
            <hr>
            <p id="marcDescription"></p>

            <select id="id_mar" class="custom-select d-block w-100" required="">
              <option value="0">Selecciona una opción...</option>
              <?php getMarca(); ?>
            </select>
            <input type="hidden" id="id_prospecto" value="" />

          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>

            <button type="button" class="btn btn-success" onclick="selectMarc()">Select</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- EDITH MODAL -->
  
  <div class="modal fade" id="edithTechnic" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title">Reasignar Técnico</h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
          <!-- Modal body -->
          <div class="modal-body">
            <p id="technicText"></p>
            <hr>
            <p id="technicDescription"></p>

            <select id="id_emp" class="custom-select d-block w-100" required="">
              <option value="0">Selecciona un técnico...</option>
              <?php getEmpleados(); ?>
            </select>
            <input type="hidden" id="id_servic" value="" />

          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>

            <button type="button" class="btn btn-success" onclick="selectTechnic()">Select</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- EDITH MODAL -->
  
  <div class="modal fade" id="showInfoService" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title">Información el Servicio</h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
          <!-- Modal body -->
          <div class="modal-body">
            <p id="modalInfoService"></p>
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>

            <button type="button" class="btn btn-success" onclick="selectTechnic()">Select</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <!-- SUCCESS / FAIL ACTION MODAL -->
  
  <div class="modal fade" id="actionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="successModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
          <!-- Modal body -->
          <div class="modal-body">
            <p id="successModalDescription"></p>
          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
            <button class="btn btn-primary" type="button" data-dismiss="modal">Continuar</button>
          </div>
        </form>
      </div>
    </div>
  </div>

   <!-- GUARANTEE MODAL -->

      <div class="modal fade" id="activarGarantia" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h2 class="modal-title">Activar Garantia</h2>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form>
              <!-- Modal body -->
              <div class="modal-body">
                <p>Ingresa los datos necesarios para activar la garantia, folio <b id="folio"></b>.</p>
                <hr>
                <form>
                  <p><b>¿Cuando fallo el equipo?</b></p>
                  <input type="date" class="form-control" name="primer_falla"><br>
                  <p><b>¿Que falla esta presentando el equipo?</b></p>
                  <input type="text" class="form-control" name="" placeholder="Ej: El equipo no enciende..."><br>
                  <p><b>¿Hablo primero con el técnico?</b></p>
                  <input type="radio" id="html" name="fav_language" value="HTML"> <label for="html">Si</label> <input type="radio" id="css" name="fav_language" value="CSS"> <label for="css">No</label><br>

                </form>

              </div>

              <!-- Modal footer -->
              <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                <button class="btn btn-success" type="button" data-dismiss="modal">Activar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

</div>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <!-- EXAMPLE BUTTONS DATATABLES 
    
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

  -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  
  <?php include 'footer.php';?>

</body>

</html>
<?php

?>