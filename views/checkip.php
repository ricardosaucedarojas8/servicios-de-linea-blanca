<?php 
date_default_timezone_set('America/Mexico_City');

if(isset($_GET['suc']) && !empty($_GET['suc'])){
  
  $success = $_GET['suc'];
  $id_usuario = $_GET['idu'];
}else{
  $success = false;
  $id_usuario = false;
}

?>
<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <!-- Custom fonts for this template-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="../css/alert.css" rel="stylesheet">
  <link href="../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
  
  <title>Seguimiento de IPS</title>
</head>

<body class="bg-light">

  <div class="container">
    <div class="py-5 text-center">
      <img class="d-block mx-auto mb-4" src="../img/logotipo-morado.png" alt="" width="100" >
      <h2>Seguimiento de IP's</h2>
      <p class="lead">Aqui podemos ver con claridad las personas que entran a todas nuestras web.</p>
    </div>


    <div class="row">
      <div class="table-responsive">
        <table id="checkip" class="display nowrap" style="width:100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>Seguimiento de IP</th>
              <th># de visitas</th>
              <th>Fecha de la visita</th>
              <th>Página visitada</th>
              <th>Status</th>
              <th>Verificar</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Seguimiento de IP</th>
              <th># de visitas</th>
              <th>Fecha de la visita</th>
              <th>Página visitada</th>
              <th>Status</th>
              <th>Verificar</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    
    <!-- Start Global Footer-->

    <?php require_once 'footer.php'; ?>

    <!-- End Global Footer-->

    <!-- Position it -->
    <div class="notice-position">

      <!-- Then put toasts within -->

      <div class="toast success bg-primary hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000" id="success">
        <div class="toast-header">
          <strong class="mr-auto">¡Bien Hecho!</strong>
          <small class="text-muted">justo ahora</small>
          <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="toast-body text-white">
          Has copiado la IP al dar clic en el botón con éxito.
        </div>
      </div>
      
    </div>

  </div>

  
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <script src="../js/checkip.js"></script>

  
</body>
</html>

<?php ?>