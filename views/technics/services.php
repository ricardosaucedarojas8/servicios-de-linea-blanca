<?php 

$id_emp;


if(isset($_GET['ser']) && !empty($_GET['ser'])){

  $id_emp = $_GET['ser'];
}else{
  $id_emp = false;
}


function getServicios($id_emp){ 

  require_once '../../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();



  $sql = 'SELECT p.nombre_completo, p.direccion, p.telefono, p.correo, e.nombre, m.nombre_marca, p.fecha_de_visita, p.hora_de_visita, p.diagnostico, s.id_servicio, m.img_marca, s.id_prospecto, s.status, s.precio, s.anticipo, emp.nombre, emp.apellidos, s.id_tipo_servicio, s.folio_garantia, s.fecha_entrega, p.cp, s.comision FROM servicios s INNER JOIN empleado emp ON s.id_empleado = emp.id_empleado INNER JOIN prospectos p ON p.id_prospecto = s.id_prospecto INNER JOIN marca m ON p.id_marca = m.id_marca INNER JOIN equipo e ON p.id_equipo = e.id_equipo WHERE s.id_empleado = '.$id_emp.' AND emp.status = 1 ORDER BY s.id_servicio DESC';

  $result = mysqli_query($connection, $sql);

  /* fetch associative array */
  while ($row = mysqli_fetch_row($result)) {

    $direccion = $row[1];

    echo'<div class=" col-md-4">';
    echo'<div class="card">';
    echo'<img class="card-img-top" src="../../img/'.$row[10].'" alt="Card image cap">';
    echo'<div class="card-body">';
    echo'<h5 class="card-title text"><b>'.$row[0].'</b></h5>';
    echo'<p class="card-text" style="font-size:12px !important;">'.$row[4].'  <b>('.$row[5].')</b><br><b>Folio: p'.$row[11].'</b><br><b>Ubicación:</b> cp '.$row[20].' - '.$row[1].' <br> <b>Antecedentes:</b> '.$row[8].'</p>';
    echo'</div>';
    echo'<ul class="list-group list-group-flush">';
    echo'<li class="list-group-item">Visitar: <b>'.$row[6].' | '.$row[7].' hrs </b></li>';
    echo'<li class="list-group-item">Llamar: <a href="tel:+52'.$row[2].'" class="contactphone"><b>'.$row[2].'</b></a><br>';
    echo'</li>';
    echo'<li class="list-group-item">Whatsapp: <a href="https://wa.me/52'.$row[2].'" class="contactphone"><b>'.$row[2].'</b></a><br>';
    echo'</li>';
    echo'</ul>';
    echo'<div class="card-body">';
    echo '<form action="confirm.php?ser='.$id_emp.'" id="confirm" method="post" enctype="multipart/form-data">';
    echo'<input type="hidden" value="'.$row[5].'" id="marca"  name="marca">';
    echo'<input type="hidden" value="'.$row[3].'" id="correo"  name="correo">';
    echo'<input type="hidden" value="'.$row[2].'" id="telefono"  name="telefono">';
    echo'<input type="hidden" value="'.$id_emp.'" id="id_empleado"  name="id_empleado">';
    echo'<input type="hidden" value="'.$row[4].'" id="equipo"  name="equipo">';
    echo'<input type="hidden" value="'.$row[0].'" id="nombre"  name="nombre">';
    echo'<input type="hidden" value="p'.$row[11].'" id="folio"  name="folio">';
    echo'<input type="hidden" value="'.$row[21].'" id="comision"  name="comision">';
    echo'<input type="hidden" value="'.$row[9].'" id="id_servicio"  name="id_servicio">';
    echo'<input type="hidden" value="'.$row[11].'" id="id_prospecto"  name="id_prospecto">';
    if($row[12] == 1){
      echo'<input type="submit" class="btn btn-success" style="width:100%;" value="Iniciar Servicio">';
    }else if($row[12] == 2){
      echo'<a onclick="terminarServicio(this)" class="btn btn-primary" style="width:100%;" data-id-servicio="'.$row[9].'" data-anticipo="'.$row[14].'" data-precio="'.$row[13].'" data-correo="'.$row[3].'" data-nombre-completo="'.$row[0].'" data-folio="p'.$row[11].'" data-equipo="'.$row[4].'" data-marca="'.$row[5].'" data-fecha-entrega="'.$row[19].'" data-id-tipo-servicio="'.$row[17].'" data-folio-garantia="'.$row[18].'" data-comision="'.$row[21].'" >Por Terminar</a>';
    }else if($row[12] == 3){
      echo'<span class="btn btn-info" style="width:100%;">Finalizado</a>';
    }else if($row[12] == 4){
      echo'<a class="btn btn-danger" style="width:100%;">Pendiente</a>';
    }
    echo'</form>';
    echo'</div>';
    echo'</div><br>';
    echo'</div>';


  }

}

?>
<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <!-- Custom fonts for this template-->
  
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="../../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  <script src="../../js/servicio.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

  <title>Servicios</title>

  <script type="text/javascript">
    // Función de javascript para ejecutar repetidamente
    window.setInterval(
      function(){
        // Sección de código para modificar el DIV
        // $("#miDiv").text(variable);

        // Ejemplo: Cada dos segundos se imprime la hora
        $("#servic").load("../../views/technics/services.php?ser=<?php echo''.$id_emp; ?>  #servic");
        
      }
      // Intervalo de tiempo
      ,60000);
    </script>

  </head>
  <body class="bg-light">

    <div class="container">
      <div class="col-md-12">
        <div class="py-5 text-center">
          <img class="d-block mx-auto mb-4" src="../../img/logotipo-morado.png" alt="" width="100" >
          <h2>Servicios</h2>
          <p class="lead">Bienvenido, en esta sección podrás ver la lista de Servicios que han sido asignados para ti.</p>
        </div>
        <span id="servic">
          <div class="row">


           <?php getServicios($id_emp); ?>

         </div> 
       </span>
     </div>    
   </div>

   <!-- SUCCESS / FAIL ACTION MODAL -->

   <div class="modal fade" id="actionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="successModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
          <!-- Modal body -->
          <div class="modal-body">
            <p id="successModalDescription"></p>
          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="confirmEnd(<?php echo ''.$id_emp; ?>)">Agree</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- END SUCCESS / FAIL ACTION MODAL -->

  <!-- PROCESS / ANTICIPO -->

  <div class="modal fade" id="endProcess" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="endModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form id="formConfirm" class="needs-validation" novalidate>
          <!-- Modal body -->
          <div class="modal-body">
            <p id="endModalDescription"></p>
            <label id="labelPending" style="color:red;"></label>
            <input type="hidden" value="" id="id_servic" name="id_servic">
            <input type="hidden" value="" id="nombre_completo" name="nombre_completo">
            <input type="hidden" value="" id="email" name="email">
            <input type="hidden" value="" id="nombre_marca" name="nombre_marca">
            <input type="hidden" value="" id="tipo_equipo" name="tipo_equipo">
            <input type="hidden" value="" id="fecha_entrega" name="fecha_entrega">
            <input type="hidden" value="" id="num_folio" name="num_folio">
            <input type="hidden" value="" id="folio_garantia" name="folio_garantia">
            <input type="hidden" value="" id="id_tipo_servicio" name="id_tipo_servicio">
            <input type="hidden" value="" id="pendiente" name="pendiente">
            <p id="pendingAlert" class="text-danger hide"></p>
            <br>
            <div>
              <label>Confirma la cantidad a liquidar:</label>
              <input class="form-control" type="number"  name="confirmPendiente" id="confirmPendiente" step="0.01" placeholder="0" required>

              <div class="valid-feedback">Correcto.</div>
              <div class="invalid-feedback">Ingresa la cantidad correcta. Min 5 caracteres. No usar (*%/$+-.,)</div>
            </div>
            <div>
              <label>Gastos en refacciones:</label>
              <input class="form-control" type="number"  name="refacciones" id="refacciones" step="0.01" placeholder="0" required>

              <div class="valid-feedback">Correcto.</div>
              <div class="invalid-feedback">Ingresa una cantidad correcta, este campo no debe estar vacio. No usar (*%/$+-.,)</div>

            </div>
          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="confirmEndService()">Liquidar Servicio</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- END PROCESS / ANTICIPO -->

  <?php include 'footer.php';?>
  
</div>

    <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <!-- Bootstrap core JavaScript-->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>


    </body>
    </html>

    <?php ?>
