<?php 

$id_emp;


if(isset($_GET['ser']) && !empty($_GET['ser'])){

  $id_emp = $_GET['ser'];
}else{
  $id_emp = false;
}



date_default_timezone_set('America/Mexico_City');

function getTipoServicio(){ 
  require_once '../../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM tipo_servicio WHERE ts_status = 1 ORDER BY ts_nombre";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_tipo_servicio'].'"> '.$row['ts_nombre'].'</option>';    
  }

}

function getMetodoPago(){ 
  require_once '../../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM tipo_metodo_pago WHERE status = 1 ORDER BY nombre_pago";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_metodo_pago'].'"> '.$row['nombre_pago'].'</option>';    
  }

}

?>

<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="../../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  <script src="../../js/servicio.js"></script>
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
  
</head>
<body class="bg-light">

  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <br><br>
        <img class="d-block mx-auto mb-4" src="../../img/logotipo-morado.png" alt="" width="100" >
        <h2>Verificar Servicio</h2>
        <p class="lead">A continuación escribe la descripcion y sus caracteristicas principales del servicio.</p>
        <br>
        <hr>

      </div>
      <div class="col-md-4">
        <b class="design-label"><?php echo ''.$_REQUEST['nombre']; ?></b>
      </div>
      <div class="col-md-4">
        <span class="label-design"><?php echo ''.$_REQUEST['equipo'].' (<b>'.$_REQUEST['marca'] .'</b>)'; ?></span>
      </div>
      <div class="col-md-4">
        <span class="label-design">Folio del Servicio: <b><?php echo ''.$_REQUEST['folio']; ?></b></span>
      </div>
      <div class="col-md-12">
        <hr><br>
      </div>
    </div>

    <form id="formService" class="needs-validation" novalidate>
      <div class="row">

        <div class="col-md-6 mb-3">
          <label for="firstName" class="label-design"><b>Folio de la Garantía</b></label>
          <input type="text" class="form-control" id="folio_garantia" placeholder="Ej: N01XX" value="<?php echo ''.$_REQUEST['folio']; ?>" required>
          <div class="invalid-feedback" id="errorFolio">
            Folio is required.
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="date" class="label-design"><b>Fecha de la Entrega</b></label>
          <input type="date" class="form-control" name="fecha_entrega" id="fecha_entrega" placeholder="" value="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" required>
          <div class="invalid-feedback" id="errorDate">
            Delivery Date is required.
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="date" class="label-design"><b>Tipo de Servicio</b></label>
          <select class="custom-select d-block w-100" id="id_tipo_servicio" name="id_tipo_servicio" required>
            <option value="">Elige una opción...</option>
            <?php getTipoServicio(); ?>
          </select>
          <div class="invalid-feedback" id="errorService">
            Service Type is required.
          </div>
        </div>

        <div class="col-md-6 mb-3">
          <label for="date" class="label-design"><b>¿Requiere Factura?</b></label>
          <br>
          <input type="radio" class="form-check-input" name="factura"  value="no" id="factura" required> <label for="factura" class="nobottom text-danger notext"><b>NO</b></label> &nbsp;
          <input type="radio" class="form-check-input" name="factura" value="si" id="facturaS" onclick="" required> <label for="facturaS" class="nobottom text-info notext"><b>SI</b></label><br>
          <div class="invalid-feedback" id="errorFactura">
            Please choose an option.
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <label for="date" class="label-design"><b>Método de Pago</b></label>
          <select class="custom-select d-block w-100" id="id_metodo_pago" name="id_metodo_pago" required>
            <option value="">Elige una opción...</option>
            <?php getMetodoPago(); ?>
          </select>
          <div class="invalid-feedback" id="errorMetod">
            Paym Method is required.
          </div>
        </div>



        <div class="col-md-6 mb-3">
          <label for="username" class="label-design"><b>Subtotal/Precio del Servicio</b></label>
          <div class="input-group">

           <input type="number" class="form-control" id="subtotal" name="subtotal" value=""  required>
           <div class="invalid-feedback" id="errorCost">
             Cost is required.
           </div>
         </div>
       </div>

       <div class="col-md-12 text-center">
        <hr>
        <br>
        <h2><b>COSTO TOTAL</b></h2>
        <p>El costo incluye descuento por ser cliente nuevo.</p>
        <!--<b><p class="display_total notext" style="color:green"></p></b>-->
        <a id="showTotal" class="btn btn-add btn-lg" style="visibility: hidden;" onclick="showTotal()">Ver costo</a>
        <br>
        <hr>
      </div>

      <div class="col-md-6 mb-3">
       <label for="correo" class="label-design"><b>Anticipo del 30% o superior</b></label>
       <!-- <p class="display lead" style="color:green"></p> -->
       <div class="input-group">

         <input type="number" class="form-control" id="adelanto" name="adelanto" value="" step="0.01" required>
         <div class="invalid-feedback" id="errorAdelanto">
           Advancemenet is required.
         </div>
       </div>

     </div>
     <div class="col-md-6 mb-3">
      <label for="correo" class="label-design"><b>Precio con Factura (16% IVA)</b></label>
      <b><p class="display_fact notext" style="color:orange"></p></b>
    </div>

    <input type="hidden" value="<?php echo ''.$_REQUEST['marca']; ?>" id="marca"  name="marca">
    <input type="hidden" value="<?php echo ''.$_REQUEST['correo']; ?>" id="correo"  name="correo">
    <input type="hidden" value="<?php echo ''.$_REQUEST['equipo']; ?>" id="equipo"  name="equipo">
    <input type="hidden" value="<?php echo ''.$_REQUEST['nombre']; ?>" id="nombre"  name="nombre">
    <input type="hidden" value="<?php echo ''.$_REQUEST['id_prospecto']; ?>" id="id_prospecto"  name="id_prospecto">
    <input type="hidden" value="<?php echo ''.$_REQUEST['id_servicio']; ?>" id="id_servicio"  name="id_servicio">
    <input type="hidden" value="<?php echo ''.$_REQUEST['folio']; ?>" id="folio"  name="folio">
    <input type="hidden" value="" id="precio_fact"  name="precio_fact">
    <input type="hidden" value="" id="costo"  name="costo">
    <input type="hidden" value="" id="comision"  name="comision">
    <input type="hidden" name="id_empleado" id="id_empleado" value="<?php echo ''.$_REQUEST['id_empleado']; ?>">
    <br>

    <div class="col-md-6 mb-12">
      <label for="diagnostic" class="label-design"><b>Diagnostico Final</b></label>
      <textarea id="diagnostico_final" class="form-control" name="diagnostico_final" placeholder="" required></textarea>
      <div class="invalid-feedback" id="errorDiagnostic">
        Please enter your diagnostic.
      </div>
      <br>
    </div>

    <div class="col-md-6 mb-3">
      <label for="correo" class="label-design"><b>El cliente pago:</b></label>
      <br>
      <input class="form-check-input" type="radio" name="pago" id="anti" value="anticipo" onclick="hideTFR()"  required> <label for="anti" class="text-primary nobottom"><b>Anticipo</b></label><br>
      <input class="form-check-input" type="radio" name="pago" id="completo" value="completo" onclick="showTFR()"  required> <label for="completo" class="text-success nobottom"><b>Servicio Completo</b></label><br>
      <input class="form-check-input" type="radio" name="pago" id="revision" value="revision" onclick="hideTFR()"  required> <label for="revision" class="text-danger nobottom"><b>Revisión</b></label> 
      <div class="invalid-feedback" id="errorPago">
        Please choose an option.
      </div>
    </div>

    <div class="col-md-12">
      <hr><br>
    </div>

    <div class="col-md-6 mb-3" id="divRefaccion" style="display:none">
      <label for="diagnostic" class="label-design"><b>Gastos de refacciones</b></label>
      <input type="number" class="form-control" id="totRefaccion" name="totRefaccion" value="0" step="0.01">
      <br>
      <div class="invalid-feedback" id="errorDiagnostic">
        Please enter total repair.
      </div>
    </div>
    <div class="col-md-6 mb-3">
    </div>

    <div class="col-md-12 mb-3">
      <a onclick="serviceC()" class="btn btn-add btn-lg btn-block">Verificar</a>
    </div>


  </div>
</form>

</div>


<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog bg-success text-white text-center signUpModal">
        <div class="modal-content bg-success">
          <div class="modal-header text-center">
            <h3 class="modal-title" id="exampleModalLabel"><b>Gran Oferta</b></h3>
          </div>
          <div class="modal-body ">
            <p class="lead">Costo total por ser primer servicio:</p>
            <div>
              <b><span class="display_total totalSuccess dot bg-warning"></span></b>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR </button>
          </div>
        </div>
      </div>
    </div>


<!-- SUCCESS / FAIL ACTION MODAL -->

<div class="modal fade" id="actionModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h2 class="modal-title"><p id="successModalTitle"></p></h2>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form>
        <!-- Modal body -->
        <div class="modal-body">
          <p id="successModalDescription"></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <input type="hidden" value="<?php echo ''.$_REQUEST['id_empleado']; ?>" id="id_empleado" name="id_empleado">
          <button type="button" class="btn btn-success" onclick="confirmService(<?php echo ''.$_REQUEST['id_empleado']; ?>)">Agree</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php include 'footer.php';?>

</div>

    <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->


      <!-- Bootstrap core JavaScript-->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

      <!-- Custom scripts for all pages-->
      <script src="../../js/sb-admin-2.min.js"></script>

    </body>
    </html>

    <?php ?>