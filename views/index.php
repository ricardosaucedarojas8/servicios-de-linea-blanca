<?php 
date_default_timezone_set('America/Mexico_City');

if(isset($_GET['suc']) && !empty($_GET['suc'])){

  $success = $_GET['suc'];
  $id_usuario = $_GET['idu'];
}else{
  $success = false;
  $id_usuario = false;
}

function countProspectos($id_usuario){

  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT count(id_prospecto) as total FROM prospectos WHERE status = 1 ";

  $result = mysqli_query($connection, $sql);

  $data=mysqli_fetch_assoc($result);

  $total_pendientes = $data['total'];

  return $total_pendientes;
}

function getProspectos($id_usuario){ 
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM prospectos WHERE status = 1  ORDER BY id_prospecto DESC";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){

    echo'<li class="list-group-item d-flex justify-content-between lh-condensed">';
    echo '<div>';
    echo'<h6 class="my-0 name-prospect usr"> '.$row['nombre_completo'].'</h6>';
    echo'<small class="folio">Folio: <b class="text-primary">p'.$row['id_prospecto'].'</b></small><br>';
    echo'<small class="text-muted visit">'.$row['fecha_de_visita'].'</small><br>';
    echo'<small class="text-muted hour">'.$row['hora_de_visita'].' hrs</small><br>';
    echo'<a onclick="showProspect(this)" data-nombre-completo="'.$row['nombre_completo'].'" data-empresa="'.$row['empresa'].'" data-tel="'.$row['telefono'].'" data-correo="'.$row['correo'].'" data-direc="'.$row['direccion'].'" data-cp="'.$row['cp'].'" data-fecha-visita="'.$row['fecha_de_visita'].'" data-hora="'.$row['hora_de_visita'].'" data-id-marca="'.$row['id_marca'].'" data-id-equipo="'.$row['id_equipo'].'" data-diagnostico="'.$row['diagnostico'].'" data-id="'.$row['id_prospecto'].'" data-id-usuario="'.$id_usuario.'"><small class="text-warning edithtext"><b>Editar</b></small></a>';
    echo'</div>';


    echo '<a  onclick="asignarServicio(this)" data-toggle="tooltip" style="color:white !important;" data-placement="top" data-nombre-completo="'.$row['nombre_completo'].'" data-empresa="'.$row['empresa'].'" data-tel="'.$row['telefono'].'" data-correo="'.$row['correo'].'" data-direc="'.$row['direccion'].'" data-cp="'.$row['cp'].'" data-fecha-visita="'.$row['fecha_de_visita'].'" data-hora="'.$row['hora_de_visita'].'" data-id-marca="'.$row['id_marca'].'" data-id-equipo="'.$row['id_equipo'].'" data-diagnostico="'.$row['diagnostico'].'" data-id="'.$row['id_prospecto'].'" data-id-usuario="'.$id_usuario.'" class="btn btn-success btn-circle see-service"><i class="fa fa-eye" aria-hidden="true"></i></a>';

    echo'</li>';

  }

}

function getMarca(){ 
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM marca WHERE status = 1 ORDER BY nombre_marca";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_marca'].'"> '.$row['nombre_marca'].'</option>';    
  }

}

function getSucursal(){ 
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM sucursales WHERE suc_status = 1 ORDER BY suc_nombre";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_sucursal'].'"> '.$row['suc_nombre'].'</option>';    
  }

}

function getEquipo(){ 
  require_once '../controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();

  $sql = "SELECT * FROM equipo WHERE status = 1 ORDER BY id_equipo";

  $result = mysqli_query($connection, $sql);
  $tabla = "";

  while($row = mysqli_fetch_array($result)){
    echo'<option value="'.$row['id_equipo'].'"> '.$row['nombre'].'</option>';    
  }

}

?>
<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <!-- Custom fonts for this template-->

  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  <script src="../js/prospecto.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
  <title>Prospectos</title>

  <?php if($success==="true"){ ?>
    <script>
      $(document).ready(function(){
        $("#mostrarmodal").modal("show");
      });
    </script>
  <?php } ?>

  <script type="text/javascript">
    // Función de javascript para ejecutar repetidamente
    window.setInterval(
      function(){
        // Sección de código para modificar el DIV
        // $("#miDiv").text(variable);

        // Ejemplo: Cada dos segundos se imprime la hora
        $("#num_prospectos").load("../views/index.php?suc=true&idu=<?php echo''.$id_usuario; ?>  #num_prospectos");
        $("#propectos_refresh").load("../views/index.php?suc=true&idu=<?php echo''.$id_usuario; ?> #propectos_refresh");
      }
      // Intervalo de tiempo
      ,60000);
    </script>
  </head>

  <body class="bg-light">

    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="../img/logotipo-morado.png" alt="" width="100" >
        <h2>Prospectos</h2>
        <p class="lead">Captura los datos que a continuación se requieren, todos los datos con * son obligatorios.</p>
      </div>

      <div class="row">

        <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Información Importante</h4>

          <form id="form_prospect" class="needs-validation" novalidate>

            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="firstName">* Nombre Completo</label>
                <input type="text" class="form-control" id="nombre" placeholder="Juan Manuel" minlength="5" maxlength="40" required pattern="^( ?\.?[a-zA-Z0-9áéíóú-]+)+$">

                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback">Ingresa un nombre correcto. Min 5 Max 50 caracteres. No usar (*%/$+-.,)</div>

              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">* Empresa</label>
                <input type="text" class="form-control" id="empresa" placeholder="Mobilia" value="Particular" minlength="5" maxlength="40" required pattern="^( ?\.?[a-zA-Z0-9áéíóú-]+)+$">

                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback" id="errorEnterprise">
                Ingresa un dato correcto. Min 5 Max 50 caracteres. No usar (*%/$+-.,)</div>

              </div>

           

            <div class=" col-md-12 mb-3">
              <label for="username">* Teléfono (Ej: 5567457335)</label>
              <div class="input-group">

                <input type="tel" class="form-control" id="telefono" maxlength="10" placeholder="5567746375"  pattern="[0-9]{10}" required>

                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback" id="errorPhone">Ingresa un teléfono correcto de 10 dígitos. No usar letras o (*%/$+-.,)</div>

              </div>
            </div>

            <div class=" col-md-12 mb-3">
              <label for="correo">* Correo</label>
              <input type="email" class="form-control" id="correo" placeholder="you@example.com" required>

              <div class="valid-feedback">Correcto.</div>
              <div class="invalid-feedback">Ingresa un correo con formato correcto.</div>

            </div>

            <div class="col-md-9 mb-3">
              <label for="address">* Dirección</label>
              <input type="text" class="form-control" id="direccion" placeholder="Av del Mazo #..."  minlength="5" maxlength="300" required pattern="^( ?\.?[a-zA-Z0-9áéíóú-]+)+$">

              <div class="valid-feedback">Correcto.</div>
              <div class="invalid-feedback"> Min 5 Max 250 caracteres. No usar (*%/$+-.,)</div>

            </div>

            <div class="col-md-3 mb-3">
              <label for="zip">* CP</label>
              <input type="text" class="form-control" name="cp" id="cp" placeholder="09640" maxlength="5" pattern="[0-9]{5}" required>

              <div class="valid-feedback">Correcto.</div>
              <div class="invalid-feedback"> Min 5 números. No letras. No usar (*%/$+-.,)</div>

            </div>


              <div class="col-md-4 mb-3">
                <label for="date">* Fecha de Visita</label>
                <input type="date" class="form-control" id="fecha_visita" placeholder="" value="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" required>
                
                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback"> Seleciona una fecha.</div>

              </div>

              <div class="col-md-4 mb-3">
                <label for="state">* Hora (09:00hrs a 18hrs)</label>
                <input type="time" class="form-control" id="hr_visita"  value="<?php $date = date("H:i"); echo "$date"; ?>" required>
                
                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback"> Ingresa un horario.</div>

              </div>
              <div class="col-md-4 mb-3">
                <label for="suc">* Sucursal</label>
                <select class="custom-select d-block w-100" id="id_sucursal" required>
                  <?php getSucursal(); ?>
                </select>
                
                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback">Selecciona una sucursal.</div>

              </div>
              
              <div class="col-md-6 mb-3">
                <label for="country">* Marca</label>
                <select class="custom-select d-block w-100" id="id_marca" required>
                  <option value="">Elige una marca...</option>
                  <?php getMarca(); ?>
                </select>
                
                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback"> Seleccione la marca del equipo.</div>

              </div>

              <div class="col-md-6 mb-3">
                <label for="country">* Equipo</label>
                <select class="custom-select d-block w-100" id="id_equipo" required>
                  <option value="">Elige un equipo...</option>
                  <?php getEquipo(); ?>
                </select>
                
                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback"> Seleccione un equipo.</div>

              </div>

              <div class="col-md-12 mb-12">

                <label for="diagnostic">* Antecedentes del equipo</label>
                <input type="text" id="diagnostico" class="form-control" name="diagnostico" placeholder="El equipo tiene fallas en..."  minlength="5" maxlength="300" pattern="^( ?\.?[a-zA-Z0-9áéíóú-]+)+$" required>
                
                <div class="valid-feedback">Correcto.</div>
                <div class="invalid-feedback">Es necesario ingresar el antecedente del equipo.</div>

              </div>

            </div>

            <br>
            <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo ''.$id_usuario; ?>">
            <a onclick="sendForm()" class="btn btn-add btn-lg btn-block">Crear Prospecto</a>
          </form>
          <br>

        </div>

        <div class="col-md-4 order-md-2 mb-4" >
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Prospectos Pendientes</span>
            <span class="badge badge-secondary badge-pill"><span id="num_prospectos"><?php echo ' '.countProspectos($id_usuario).''; ?><span></span>
          </h4>
          <ul class="list-group mb-3" id="propectos_refresh">
            <?php 
            getProspectos($id_usuario);
            ?>
          </ul>

        </div>

      </div>


      <!-- SUCCESS / FAIL ACTION MODAL -->

      <div class="modal fade" id="actionModal" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h2 class="modal-title"><p id="successModalTitle"></p></h2>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form>
              <!-- Modal body -->
              <div class="modal-body">
                <p id="successModalDescription"></p>
              </div>

              <!-- Modal footer -->
              <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="confirm()">Agree</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- SUCCESS / FAIL ACTION MODAL -->

      <div class="modal fade" id="mostrarmodal" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h2 class="modal-title">Bienvenido</h2>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form>
              <!-- Modal body -->
              <div class="modal-body">
                <p>Has accedido a la plataforma con éxito.</p>
              </div>

              <!-- Modal footer -->
              <div class="modal-footer">

                <button class="btn btn-primary" type="button" data-dismiss="modal">Continuar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- EDITH PROSPECT MODAL-->

      <div class="modal fade" id="edithProspect" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h2 class="modal-title">Editar Prospecto</h2>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form>
              <!-- Modal body -->
              <div class="modal-body">
                <input type="hidden" id="id_prospecto_edt" value="">
                <input type="hidden" id="id_usuario_edt" value="">
                <input type="text" class="form-control" placeholder="Nombre Completo" id="nombre_completo_edt" value="">
                <div class="invalid-feedback" id="edithErrorName">First name is required.</div><br>
                <input type="text" placeholder="Empresa" class="form-control" id="empresa_edt" value=""><div class="invalid-feedback" id="edithErrorEnterprise">Enterprise is required.</div><br>
                <input type="tel" placeholder="Teléfono" class="form-control" id="telefono_edt" value=""><div class="invalid-feedback" id="edithErrorPhone">Phone is required.</div><br>
                <input type="email" placeholder="Correo" class="form-control" id="correo_edt" value=""><div class="invalid-feedback" id="errorEmail">Please enter a valid email address.</div><br>
                <input type="text" placeholder="Dirección" class="form-control" id="direccion_edt" value=""><div class="invalid-feedback" id="edithErrorDirection">Please enter your direction.</div><br>
                <input type="text" placeholder="CP" class="form-control" id="cp_edt" value=""><div class="invalid-feedback" id="edithErrorCp">Zip code required.</div><br>
                <input type="date" class="form-control" id="fecha_visita_edt" value=""><div class="invalid-feedback" id="edithErrorDate">Date is required.</div><br>
                <input type="time" class="form-control" id="hora_visita_edt" value=""><div class="invalid-feedback" id="edithErrorHour">Time is required.</div><br>
                <select class="custom-select d-block w-100" id="id_marca_edt" required>
                  <option value="0">Elige una marca...</option>
                  <?php getMarca(); ?>
                </select>
                <div class="invalid-feedback" id="editErrorBrand">Brand is required.</div>
                <br>
                <select class="custom-select d-block w-100" id="id_equipo_edt" required>
                  <option value="0">Elige un equipo...</option>
                  <?php getEquipo(); ?>
                </select>
                <div class="invalid-feedback" id="editErrorDevice">Device is required.</div>
                <br>
                <textarea id="diagnostico_edt" class="form-control" name="" required></textarea>
                <div class="invalid-feedback" id="edithErrorDiagnostic">Please enter your diagnostic.</div>
                <br>
              </div>

              <!-- Modal footer -->
              <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>

                <a onclick="edithProspect()" class="btn btn-warning" style="color:white;">Edith</a>

              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Start Global Footer-->
      <?php require_once 'footer.php'; ?>

      <!-- End Global Footer-->

    </div>
    <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <!-- Bootstrap core JavaScript-->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

      

    </body>
    </html>

    <?php ?>