<?php ?>
<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <link href="../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  <script src="../js/servicio.js"></script>
  <title>Calificar Servicio</title>
  <style type="text/css">
  	label {
	  display: inline-block;
	  padding: 0;
	  cursor: pointer;
	  vertical-align: middle;
	}
	label.reset {
	  font-size: 10px;
	  border: 1px solid #000;
	  border-radius: 5px;
	  margin: 10px 5px;
	  padding: 2px 3px;
	}
	label.star {
	  width: 30px;
	  height: 27px;
	}
	/* ocultamos los radiobuttons */
	input[name=rating] {
	  display: none;
	}
	/* estrellas inmediatamente despues de un radiobutton van amarillas */
	input[type=radio]+label.star svg path {
	  fill: #fe0
	}
	/* estrellas a la derecha del radiobutton checked van blanco */
	input[type=radio]:checked~label.star svg path {
	  fill: #fff;
	}
  </style>
</head>
 <body class="bg-light">

    <div class="container">
      <div class="col-md-12">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="../../img/logotipo-morado.png" alt="" width="100" >
        <h2>Calificar Servicio</h2>
        <p class="lead"><b>¿Que te parecio nuestro servicio?</b> Si tienes algún comentario u observación, este es el momento de expresarlo, recuerda que tu comentario es anónimo.</p>
      </div>



      <form id="form_quality" class="needs-validation" novalidate>
		<!-- svg from https://es.wikipedia.org/wiki/Archivo:Star*.svg -->
		<div class="form-group" style="text-align: center;">
		<input id=rating0 type=radio value=0 name=rating checked />

		<label class=star for=rating1>
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 275">
		<path stroke="#605a00" stroke-width="15" 
		  d="M150 25l29 86h90l-72 54 26 86-73-51-73 51 26-86-72-54h90z"/>
		</svg>
		</label>
		<input id=rating1 type=radio value=1 name=rating />

		<label class=star for=rating2>
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 275">
		<path stroke="#605a00" stroke-width="15"
		 d="M150 25l29 86h90l-72 54 26 86-73-51-73 51 26-86-72-54h90z"/>
		</svg>
		</label>
		<input id=rating2 type=radio value=2 name=rating />

		<label class=star for=rating3>
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 275">
		<path stroke="#605a00" stroke-width="15"
		 d="M150 25l29 86h90l-72 54 26 86-73-51-73 51 26-86-72-54h90z"/>
		</svg>
		</label>
		<input id=rating3 type=radio value=3 name=rating />

		<label class=star for=rating4>
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 275">
		<path stroke="#605a00" stroke-width="15"
		 d="M150 25l29 86h90l-72 54 26 86-73-51-73 51 26-86-72-54h90z"/>
		</svg>
		</label>
		<input id=rating4 type=radio value=4 name=rating />

		<label class=star for=rating5>
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 275">
		<path stroke="#605a00" stroke-width="15"
		 d="M150 25l29 86h90l-72 54 26 86-73-51-73 51 26-86-72-54h90z"/>
		</svg>
		</label>
		<input id=rating5 type=radio value=5 name=rating />

		<!-- por último el label del rating 0 ( sin calificar ) -->
		<label class=reset for=rating0>reset</label>

		<div id=texto>sin calificar</div>
		   </div>
      	<label for="diagnostic">* Escribe tú Comentario u Opinión</label>    
        <textarea id="diagnostico" class="form-control" name="" placeholder="" required></textarea>
        <div class="invalid-feedback" id="errorDiagnostic">
                  Please enter your diagnostic.
        </div>
        <br>
        <a onclick="doAction()" class="btn btn-add btn-lg btn-block">Calificar</a>
      </form>
         </div>
    </div>
  
  <!-- SUCCESS / FAIL ACTION MODAL -->
  
  <div class="modal fade" id="actionModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h2 class="modal-title"><p id="successModalTitle"></p></h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form>
        <!-- Modal body -->
        <div class="modal-body">
          <p id="successModalDescription"></p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" onclick="confirmEnd(<?php echo ''.$id_emp; ?>)">Agree</button>
        </div>
        </form>
      </div>
    </div>
  </div>

 <!-- END SUCCESS / FAIL ACTION MODAL -->


  <!-- END PROCESS / ANTICIPO -->

  <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2015-2020 | Home Service by Páginas Web Premium</p>
        
      </footer>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin-2.min.js"></script>
    <script type="text/javascript">
    	// para todos los radiobutton rating agregar un on change
	const changeRating = document.querySelectorAll('input[name=rating]');
	changeRating.forEach((radio) => {
	  radio.addEventListener('change', getRating);
	});

	// buscar el radiobutton checked y armar el texto con el valor ( 0 - 5 )
	function getRating() {
	  let estrellas = document.querySelector('input[name=rating]:checked').value;
	  document.getElementById("texto").innerHTML = (
	    0 < estrellas ?
	    "Calificación: " + estrellas + " estrella" + (1 < estrellas ? "s" : "") :
	    "sin calificar"
	  );

	  // opcionalmente agregar un ajax para guardar el rating
	}


    </script>

</body>
</html>

<?php ?>