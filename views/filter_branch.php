<?php 

$id_usuario;

if(isset($_GET['idu']) && !empty($_GET['idu'])){

  $id_usuario = $_GET['idu'];
}else{
  $id_usuario = false;
}

?>

<!DOCTYPE html>
<html lang="en, es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <!-- Custom fonts for this template-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="../css/form-validation.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" async></script>
  <script src="../js/servicio.js"></script>
  <script src="../js/prospecto.js"></script>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

  <title>Seguimiento de Sucursales</title>

</head>

<body class="bg-light">

  <div class="container-fluid">

    <div class="col-md-12">

      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="../img/logotipo-morado.png" alt="" width="100" >
        <h2>Servicios términados por Sucursal</h2>
        <p class="lead">A continuación se enlistan todos los servicios de todas las sucursales que han sido finalizados en sistema.</p>
      </div>

      <div class="row">
        <div class="table-responsive">
          <table id="servicesLocation" class="display nowrap" style="width:100%">
            <thead>
              <tr>
                <th>IDS</th>
                <th>Folio/Status</th>
                <th>Cliente</th>
                <th>Dirección Completa</th>
                <th>Técnico Asignado</th>        
                <th>Total</th>
                <th>Refacciones</th>
                <th>FCreación</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr>
               <th>IDS</th>
               <th>Folio/Status</th>
               <th>Cliente</th>
               <th>Dirección Completa</th>
               <th>Técnico Asignado</th>  
               <th>Total</th>
               <th>Refacciones</th>
               <th>FCreación</th>
             </tr>
           </tfoot>
         </table>
       </div>
     </div>
   </div>

 </div>

 <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

 <?php include 'footer.php';?>

</body>

</html>
